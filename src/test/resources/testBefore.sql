DELETE FROM public.item_feed_backs;
DELETE FROM public.item;

INSERT INTO public.item(id, group_name, subgroup_name, title, description, price, average_grade) VALUES
(101, 'group1', 'subgroup1', 'title', 'description', 5.05, 1),
(102, 'group1', 'subgroup1', 'title', 'description', 6.05, 2),
(103, 'group1', 'subgroup1', 'title', 'description', 7.05, 3),
(104, 'group1', 'subgroup1', 'title', 'description', 8.05, 4),
(105, 'group1', 'subgroup1', 'title', 'description', 9.05, 5),
(106, 'group1', 'subgroup1', 'title', 'description', 10.05, 1),
(107, 'group1', 'subgroup1', 'title', 'description', 11.05, 2),
(108, 'group1', 'subgroup1', 'title', 'description', 12.05, 3),
(109, 'group1', 'subgroup1', 'title', 'description', 13.05, 4),
(110, 'group1', 'subgroup2', 'title', 'description', 14.05, 5),
(111, 'group1', 'subgroup2', 'title', 'description', 15.05, 1),
(112, 'group1', 'subgroup2', 'title', 'description', 5.05, 2),
(113, 'group1', 'subgroup2', 'title', 'description', 6.05, 3),
(114, 'group1', 'subgroup2', 'title', 'description', 7.05, 4),
(115, 'group1', 'subgroup2', 'title', 'description', 8.05, 5),
(116, 'group1', 'subgroup2', 'title', 'description', 9.05, 1),
(117, 'group1', 'subgroup2', 'title', 'description', 10.05, 2),
(118, 'group1', 'subgroup2', 'title', 'description', 11.05, 3),
(119, 'group1', 'subgroup2', 'title', 'description', 12.05, 4),
(120, 'group1', 'subgroup2', 'title', 'description', 13.05, 5),
(121, 'grouSearchTestp2', 'subgroup3', 'title', 'description', 14.05, 1),
(122, 'group2', 'subgroup3', 'title', 'description', 15.05, 2),
(123, 'group2', 'subgrSearchTestoup3', 'title', 'description', 5.05, 3),
(124, 'group2', 'subgroup3', 'title', 'description', 6.05, 4),
(125, 'group2', 'subgrSearchTestoup3', 'title', 'description', 7.05, 5),
(126, 'group2', 'subgroup3', 'title', 'description', 8.05, 1),
(127, 'group2', 'subgroup4', 'titSearchTestle', 'description', 9.05, 2),
(128, 'group2', 'subgroup4', 'title', 'description', 10.05, 3),
(129, 'grouSearchTestp2', 'subgroup4', 'titSearchTestle', 'description', 11.05, 4),
(130, 'grouSearchTestp2', 'subgroup4', 'title', 'description', 12.05, 5);


INSERT INTO public.item_feed_backs(item_id, author, comment, grade)	VALUES
(101, 'author1', 'comment1', 5),
(101, 'author2', 'comment2', 4),
(101, 'author3', 'comment3', 3),
(102, 'author1', 'comment1', 5),
(102, 'author2', 'comment2', 4),
(102, 'author1', 'comment1', 2);