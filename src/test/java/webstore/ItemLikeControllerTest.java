package webstore;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import webstore.item.Item;
import webstore.item.controllers.ItemLikeController;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testDB")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:testBefore.sql")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ItemLikeControllerTest {

    @Autowired
    private ItemLikeController itemLikeController;
    @Autowired
    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(itemLikeController).build();
    }

    @Test
    public void itemCouldBeAddToLikeList() throws Exception {
        MvcResult result = mockMvc.perform(post("/api/v.0.1/like/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        Set<Item> likes = (Set<Item>) result.getRequest().getSession().getAttribute("likes");
        assertSame(1, likes.size());
        Item item = likes.stream().findFirst().get();
        assertSame(101L, item.getId());
    }

    @Test
    public void itemCouldNotBeAddToLikeListIfHasAlreadyAdded() throws Exception {
        Set<Item> likes = new HashSet<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(101L);
        likes.add(item);

        MvcResult result = mockMvc.perform(post("/api/v.0.1/like/101")
                .sessionAttr("likes", likes)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        likes = (Set<Item>) result.getRequest().getSession().getAttribute("likes");
        assertSame(1, likes.size());
        item = likes.stream().findFirst().get();
        assertSame(101L, item.getId());
        assertEquals("GR", item.getGroupName());
    }

    @Test
    public void itemCouldBeAddToLikeListWhenListNotEmpty() throws Exception {
        Set<Item> likes = new HashSet<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        likes.add(item);

        MvcResult result = mockMvc.perform(post("/api/v.0.1/like/101")
                .sessionAttr("likes", likes)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        likes = (Set<Item>) result.getRequest().getSession().getAttribute("likes");
        assertSame(2, likes.size());
    }

    @Test
    public void itemCouldNotBeAddIfItemNotExist() throws Exception {
        mockMvc.perform(post("/api/v.0.1/like/10")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    @Test
    public void itemCouldBeRemoveFromLikeList() throws Exception {
        Set<Item> likes = new HashSet<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(101L);
        likes.add(item);

        MvcResult result = mockMvc.perform(delete("/api/v.0.1/dislike/101")
                .sessionAttr("likes", likes)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        likes = (Set<Item>) result.getRequest().getSession().getAttribute("likes");
        assertSame(0, likes.size());
    }

    @Test
    public void itemCouldNotBeRemoveFromLikeListIfNotExistInLikeList() throws Exception {
        Set<Item> likes = new HashSet<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        likes.add(item);

        MvcResult result = mockMvc.perform(delete("/api/v.0.1/dislike/101")
                .sessionAttr("likes", likes)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent())
                .andReturn();

        likes = (Set<Item>) result.getRequest().getSession().getAttribute("likes");
        assertSame(1, likes.size());
    }

    @Test
    public void itemCouldNotBeRemoveFromLikeListIfListIsNullOrEmpty() throws Exception {
        Set<Item> likes = new HashSet<>();
        mockMvc.perform(delete("/api/v.0.1/dislike/101")
                .sessionAttr("likes", likes)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());

        mockMvc.perform(delete("/api/v.0.1/dislike/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    @Test
    public void likeListCouldBeGet() throws Exception {
        Set<Item> likes = new HashSet<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        likes.add(item);

        MvcResult result = mockMvc.perform(get("/api/v.0.1/likes")
                .sessionAttr("likes", likes)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJson = result.getResponse().getContentAsString();
        Item[] items = mapper.readValue(itemsJson, Item[].class);
        assertSame(1, items.length);
        assertSame(10L, items[0].getId());
    }

    @Test
    public void likeListCouldBeGetIfListisNull() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v.0.1/likes")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJson = result.getResponse().getContentAsString();
        Item[] items = mapper.readValue(itemsJson, Item[].class);
        assertSame(0, items.length);
    }

    @Test
    public void likeListCouldBeRemoved() throws Exception {
        Set<Item> likes = new HashSet<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        likes.add(item);

        MvcResult result = mockMvc.perform(delete("/api/v.0.1/likes")
                .sessionAttr("likes", likes)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        likes = (Set<Item>) result.getRequest().getSession().getAttribute("likes");
        assertSame(0, likes.size());
    }
}
