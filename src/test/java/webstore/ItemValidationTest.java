package webstore;

import org.junit.BeforeClass;
import org.junit.Test;
import webstore.item.Item;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ItemValidationTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void CheckIsNullOrIsEmpty() {
        Item item = new Item();
        Set<ConstraintViolation<Item>> constraintViolations = validator.validate(item);
        assertEquals(5, constraintViolations.size());

        String err1 = "may not be null";
        String err2 = "may not be empty";

        for (int i = 0; i < constraintViolations.size(); i++) {
            String message = constraintViolations.iterator().next().getMessage();
            assertTrue(message.equals(err1)||message.equals(err2));
        }

    }

    @Test
    public void priceMustBeGreaterThen0() {
        Item item = new Item();
        item.setGroupName("group");
        item.setSubgroupName("subgroup");
        item.setTitle("title");
        item.setDescription("desc");
        item.setPrice(-1.0);
        Set<ConstraintViolation<Item>> constraintViolations = validator.validate(item);
        assertEquals(1, constraintViolations.size());
        assertEquals("must be greater than or equal to 0", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validItem() {
        Item item = new Item();
        item.setGroupName("group");
        item.setSubgroupName("subgroup");
        item.setTitle("title");
        item.setDescription("desc");
        item.setPrice(1.2);
        Set<ConstraintViolation<Item>> constraintViolations = validator.validate(item);
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void averageGradeMustBeGrOrEqThan0AndLessOrEqThan5() {
        Item item = new Item();
        item.setGroupName("group");
        item.setSubgroupName("subgroup");
        item.setTitle("title");
        item.setDescription("desc");
        item.setPrice(1.2);

        item.setAverageGrade(-1.0);
        Set<ConstraintViolation<Item>> constraintViolations = validator.validate(item);
        assertEquals(1, constraintViolations.size());
        assertEquals("must be greater than or equal to 0", constraintViolations.iterator().next().getMessage());

        item.setAverageGrade(6.0);
        constraintViolations = validator.validate(item);
        assertEquals(1, constraintViolations.size());
        assertEquals("must be less than or equal to 5", constraintViolations.iterator().next().getMessage());
    }

}
