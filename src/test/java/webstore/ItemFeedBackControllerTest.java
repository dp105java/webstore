package webstore;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import webstore.item.FeedBack;
import webstore.item.controllers.ItemFeedBackController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testDB")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:testBefore.sql")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ItemFeedBackControllerTest {

    @Autowired
    private ItemFeedBackController itemFeedBackController;
    @Autowired
    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(itemFeedBackController).build();
    }

    @Test
    public void feedBackCouldBeGetByItemId() throws Exception {
        mockMvc.perform(get("/api/v.0.1/feedBack/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
    }

    @Test
    public void feedBackCouldNotBeGetByItemIdIfNoFeedBack() throws Exception {
        mockMvc.perform(get("/api/v.0.1/feedBack/110")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    @Test
    public void feedBackCouldNotBeGetByItemIdIfNotExist() throws Exception {
        mockMvc.perform(get("/api/v.0.1/feedBack/10")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    @Test
    public void feedBackCouldBeAdd() throws Exception {
        FeedBack feedBack = new FeedBack("author", "comment", 4);

        mockMvc.perform(post("/api/v.0.1/feedBack/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(mapper.writeValueAsString(feedBack))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isCreated());
    }

    @Test
    public void feedBackCouldNotBeAddIfItemNotExist() throws Exception {
        FeedBack feedBack = new FeedBack("author", "comment", 4);

        mockMvc.perform(post("/api/v.0.1/feedBack/10")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(mapper.writeValueAsString(feedBack))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void feedBackCouldNotBeAddIfNotValid() throws Exception {
        FeedBack feedBack = new FeedBack("author", "comment", 6);

        mockMvc.perform(post("/api/v.0.1/feedBack/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(mapper.writeValueAsString(feedBack))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isBadRequest());
    }

}
