package webstore;

import org.junit.BeforeClass;
import org.junit.Test;
import webstore.item.FeedBack;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FeedBackValidationTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void CheckIsNullOrIsEmpty() {
        FeedBack feedBack = new FeedBack();
        feedBack.setGrade(0);

        Set<ConstraintViolation<FeedBack>> constraintViolations = validator.validate(feedBack);
        assertEquals(2, constraintViolations.size());

        for (int i = 0; i < constraintViolations.size(); i++) {
            String message = constraintViolations.iterator().next().getMessage();
            assertTrue(message.equals("may not be empty"));
        }
    }

    @Test
    public void gradeMustBeGrOrEqThan0AndLessOrEqThan5() {
        FeedBack feedBack = new FeedBack("author", "comment", -1);

        Set<ConstraintViolation<FeedBack>> constraintViolations = validator.validate(feedBack);
        assertEquals(1, constraintViolations.size());
        assertEquals("must be greater than or equal to 0", constraintViolations.iterator().next().getMessage());

        feedBack.setGrade(6);
        constraintViolations = validator.validate(feedBack);
        assertEquals(1, constraintViolations.size());
        assertEquals("must be less than or equal to 5", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void validFeedBack() {
        FeedBack feedBack = new FeedBack("author", "comment", 3);
        Set<ConstraintViolation<FeedBack>> constraintViolations = validator.validate(feedBack);
        assertEquals(0, constraintViolations.size());
    }
}
