package webstore;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import webstore.item.FeedBack;
import webstore.item.Item;
import webstore.item.ItemRepository;
import webstore.item.ItemService;

import java.util.List;

import static org.junit.Assert.*;
import static webstore.item.ItemServiceImpl.PAGE_SIZE;


@SpringApplicationConfiguration(Application.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testDB")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:testBefore.sql")
@RunWith(SpringJUnit4ClassRunner.class)
public class ItemServiceIntegrationTest {
    @Autowired
    private ItemService itemService;
    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void ItemCouldBeSavedAndDelete() {
        Item item = new Item("G", "S", "T", "D", 5.05, 4);
        int countBeforeSaving = itemRepository.findAll().size();
        assertTrue(itemService.saveOrUpdate(item));
        int countAfterSaving = itemRepository.findAll().size();
        assertSame(countBeforeSaving + 1, countAfterSaving);

        assertTrue(itemService.delete(1L));
        int countAfterDeleting = itemRepository.findAll().size();
        assertSame(countBeforeSaving, countAfterDeleting);
    }

    /*
     *  When item was updated, List<FeedBack> and averageGrade should not be updated.
     */
    @Test
    public void ItemCouldBeUpdated() {
        Item item = itemService.getById(101L);
        int countOfFeedBacks = item.getFeedBacks().size();
        double averageGrade = item.getAverageGrade();

        String t = "newTitle";
        item.setTitle(t);
        item.getFeedBacks().add(new FeedBack("author", "comment", 0));
        item.setAverageGrade(0);
        assertTrue(itemService.saveOrUpdate(item));
        item = itemService.getById(101L);
        ;
        assertEquals(t, item.getTitle());
        assertSame(countOfFeedBacks, item.getFeedBacks().size());
        assertEquals(averageGrade, item.getAverageGrade(), 0.1);
    }

    @Test
    public void NullCouldNotBeSavedOrUpdatedOrDelete() {
        assertFalse(itemService.saveOrUpdate(null));
        assertFalse(itemService.delete(null));
    }

    @Test
    public void ItemCouldNotBeSavedOrUpdatedIfIdIsNotNullButItIsNotExistInDB() {
        Item item = new Item("G", "S", "T", "D", 5.05, 4);
        item.setId(5L);
        assertFalse(itemService.saveOrUpdate(item));
    }

    @Test
    public void ItemCouldNotBeDeletedIfNotExist() {
        assertFalse(itemService.delete(10L));
    }

    @Test
    public void ItemCouldBeGot() {
        Long id = 101L;
        Item item = itemService.getById(id);
        assertEquals(id, item.getId());
    }

    @Test
    public void ItemCouldNotBeGotIfIdIsNullOrNotExist() {
        Long id = 11L;
        assertNull(itemService.getById(id));
        assertNull(itemService.getById(null));
    }

    @Test
    public void ItemsCouldBeGotByGroupName() {
        String group = "group1";
        Page<Item> items = itemService.getItems(group, 0);
        assertTrue(items.hasContent());
        assertSame(PAGE_SIZE, items.getSize());
        assertSame(3, items.getTotalPages());
        assertSame(20L, items.getTotalElements());
        assertEquals(group, items.iterator().next().getGroupName());
    }

    @Test
    public void ItemsCouldNotBeGotByGroupNameIfItsNullOrNotExist() {
        String group = "group1";

        Page<Item> items = itemService.getItems(null, 0);
        assertFalse(items.hasContent());

        items = itemService.getItems("notExist", 0);
        assertFalse(items.hasContent());

        items = itemService.getItems(group, -1);
        assertFalse(items.hasContent());

        items = itemService.getItems(group, 500);
        assertFalse(items.hasContent());
    }

    @Test
    public void ItemsCouldBeGotBySubGroupName() {
        String group = "group1";
        String subgroup = "subgroup1";
        Page<Item> items = itemService.getItems(group, subgroup, 0);
        assertTrue(items.hasContent());
        assertSame(PAGE_SIZE, items.getSize());
        assertSame(1, items.getTotalPages());
        assertSame(9L, items.getTotalElements());
        assertEquals(group, items.iterator().next().getGroupName());
        assertEquals(subgroup, items.iterator().next().getSubgroupName());
    }

    @Test
    public void ItemsCouldBeGotBySubGroupNameIfItsNullOrNotExist() {
        String group = "group1";
        String subgroup = "subgroup1";

        Page<Item> items = itemService.getItems(null, null, 0);
        assertFalse(items.hasContent());

        items = itemService.getItems(group, null, 0);
        assertFalse(items.hasContent());

        items = itemService.getItems(null, group, 0);
        assertFalse(items.hasContent());

        items = itemService.getItems("notExist", "notExist", 0);
        assertFalse(items.hasContent());

        items = itemService.getItems(group, "notExist", 0);
        assertFalse(items.hasContent());

        items = itemService.getItems("notExist", subgroup, 0);
        assertFalse(items.hasContent());

        items = itemService.getItems(group, subgroup, -1);
        assertFalse(items.hasContent());

        items = itemService.getItems(group, subgroup, 500);
        assertFalse(items.hasContent());
    }

    @Test
    public void ItemsCouldBeGotByGroupNameAndFilter() {
        String group = "group1";
        int page = 0;
        int maxPrice = 10;
        int minPrice = 8;
        Sort sort = new Sort(Sort.Direction.DESC, "price");

        Page<Item> items = itemService.getItemsByFilter(group, minPrice, maxPrice, page, sort);
        assertTrue(items.hasContent());
        assertSame(PAGE_SIZE, items.getSize());
        assertSame(1, items.getTotalPages());
        assertSame(4L, items.getTotalElements());
        assertEquals(group, items.iterator().next().getGroupName());

        List<Item> itemList = items.getContent();
        Item first = itemList.get(0);
        Item last = itemList.get(3);
        assertTrue(first.getPrice() > last.getPrice());
        assertTrue(first.getPrice() < maxPrice);
        assertTrue(last.getPrice() > minPrice);
    }

    @Test
    public void ItemsCouldBeGotByGroupNameAndFilterWhenSortIsNull() {
        String group = "group1";
        int page = 0;
        int maxPrice = 10;
        int minPrice = 8;
        Sort sort = null;

        Page<Item> items = itemService.getItemsByFilter(group, minPrice, maxPrice, page, sort);
        assertTrue(items.hasContent());
        assertSame(PAGE_SIZE, items.getSize());
        assertSame(1, items.getTotalPages());
        assertSame(4L, items.getTotalElements());
        assertEquals(group, items.iterator().next().getGroupName());

        List<Item> itemList = items.getContent();
        Item first = itemList.get(0);
        Item last = itemList.get(3);
        assertTrue(first.getAverageGrade() >= last.getAverageGrade());
        assertTrue(first.getPrice() < maxPrice);
        assertTrue(first.getPrice() > minPrice);

    }

    /*
     * Sort does not fit
     * MaxPrice more than minPrice
     * Page less than 0 or more than last
     */
    @Test
    public void ItemsCouldNotBeGotByGroupNameAndFilterIfFilterNotValid() {
        String group = "group1";

        int page = 0;
        int maxPrice = 10;
        int minPrice = 8;
        Sort sort = new Sort(Sort.Direction.DESC, "motExist");
        Page<Item> items = itemService.getItemsByFilter(group, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());

        page = 0;
        maxPrice = 10;
        minPrice = 15;
        sort = new Sort(Sort.Direction.DESC, "title");
        items = itemService.getItemsByFilter(group, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());

        page = -1;
        maxPrice = 10;
        minPrice = 8;
        sort = new Sort(Sort.Direction.DESC, "title");
        items = itemService.getItemsByFilter(group, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());
    }

    @Test
    public void ItemsCouldBeGotBySubgroupNameAndFilter() {
        String group = "group1";
        String subgroup = "subgroup1";
        int page = 0;
        int maxPrice = 13;
        int minPrice = 6;
        Sort sort = new Sort(Sort.Direction.DESC, "price");

        Page<Item> items = itemService.getItemsByFilter(group, subgroup, minPrice, maxPrice, page, sort);
        assertTrue(items.hasContent());
        assertSame(PAGE_SIZE, items.getSize());
        assertSame(1, items.getTotalPages());
        assertSame(7L, items.getTotalElements());
        assertEquals(group, items.iterator().next().getGroupName());

        List<Item> itemList = items.getContent();
        Item first = itemList.get(0);
        Item last = itemList.get(6);
        assertTrue(first.getPrice() > last.getPrice());
        assertTrue(first.getPrice() < maxPrice);
        assertTrue(last.getPrice() > minPrice);

    }


    @Test
    public void ItemsCouldBeGotBySubgroupNameAndFilterWhenSortIsNull() {
        String group = "group1";
        String subgroup = "subgroup1";
        int page = 0;
        int maxPrice = 13;
        int minPrice = 6;
        Sort sort = null;

        Page<Item> items = itemService.getItemsByFilter(group, subgroup, minPrice, maxPrice, page, sort);
        assertTrue(items.hasContent());
        assertSame(PAGE_SIZE, items.getSize());
        assertSame(1, items.getTotalPages());
        assertSame(7L, items.getTotalElements());
        assertEquals(group, items.iterator().next().getGroupName());

        List<Item> itemList = items.getContent();
        Item first = itemList.get(0);
        Item last = itemList.get(6);
        assertTrue(first.getAverageGrade() >= last.getAverageGrade());
        assertTrue(first.getPrice() < maxPrice);
        assertTrue(first.getPrice() > minPrice);

    }

    /*
     * Sort does not fit
     * MaxPrice more than minPrice
     * Page less than 0 or more than last
     */
    @Test
    public void ItemsCouldNotBeGotBySubgroupNameAndFilterIfFilterNotValid() {
        String group = "group1";
        String subgroup = "subgroup1";

        int page = 0;
        int maxPrice = 10;
        int minPrice = 8;
        Sort sort = new Sort(Sort.Direction.DESC, "motExist");
        Page<Item> items = itemService.getItemsByFilter(group, subgroup, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());

        page = 0;
        maxPrice = 10;
        minPrice = 15;
        sort = new Sort(Sort.Direction.DESC, "title");
        items = itemService.getItemsByFilter(group, subgroup, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());

        page = -1;
        maxPrice = 10;
        minPrice = 8;
        sort = new Sort(Sort.Direction.DESC, "title");
        items = itemService.getItemsByFilter(group, subgroup, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());
    }

    @Test
    public void ItemsCouldBeGotBySearchSequence() {
        String search = "SeArChTesT";
        Page<Item> items = itemService.getItemsBySearch(search, 0);
        assertTrue(items.hasContent());
        assertSame(PAGE_SIZE, items.getSize());
        assertSame(1, items.getTotalPages());
        assertSame(6L, items.getTotalElements());
    }

    @Test
    public void ItemsCouldNotBeGotBySearchSequenceIfItsNullOrNotExist() {
        String search = "SeArChTesT";

        Page<Item> items = itemService.getItemsBySearch(null, 0);
        assertFalse(items.hasContent());

        items = itemService.getItemsBySearch("notExist", 0);
        assertFalse(items.hasContent());

        items = itemService.getItemsBySearch(search, -1);
        assertFalse(items.hasContent());

        items = itemService.getItemsBySearch(search, 500);
        assertFalse(items.hasContent());
    }

    @Test
    public void ItemsCouldBeGotBySearchSequenceAndFilter() {
        String search = "SeArChTesT";
        int page = 0;
        int maxPrice = 13;
        int minPrice = 6;
        Sort sort = new Sort(Sort.Direction.DESC, "price");

        Page<Item> items = itemService.getItemsBySearchAndFilter(search, minPrice, maxPrice, page, sort);
        assertTrue(items.hasContent());
        assertSame(PAGE_SIZE, items.getSize());
        assertSame(1, items.getTotalPages());
        assertSame(4L, items.getTotalElements());

        List<Item> itemList = items.getContent();
        Item first = itemList.get(0);
        Item last = itemList.get(3);
        assertTrue(first.getPrice() > last.getPrice());
        assertTrue(first.getPrice() < maxPrice);
        assertTrue(last.getPrice() > minPrice);
    }

    /*
     * Sort does not fit
     * MaxPrice more than minPrice
     * Page less than 0 or more than last
     */
    @Test
    public void ItemsCouldNotBeGotBySearchSequenceAndFilterIfFilterNotValid() {
        String search = "SeArChTesT";

        int page = 0;
        int maxPrice = 13;
        int minPrice = 6;
        Sort sort = new Sort(Sort.Direction.DESC, "motExist");
        Page<Item> items = itemService.getItemsBySearchAndFilter(search, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());

        page = 0;
        maxPrice = 13;
        minPrice = 6;
        sort = new Sort(Sort.Direction.DESC, "title");
        items = itemService.getItemsBySearchAndFilter(null, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());

        page = 0;
        maxPrice = 6;
        minPrice = 13;
        sort = new Sort(Sort.Direction.DESC, "title");
        items = itemService.getItemsBySearchAndFilter(search, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());

        page = -1;
        maxPrice = 13;
        minPrice = 6;
        sort = new Sort(Sort.Direction.DESC, "title");
        items = itemService.getItemsBySearchAndFilter(search, minPrice, maxPrice, page, sort);
        assertFalse(items.hasContent());
    }

    @Test
    public void MaxPriceCouldBeGotByGroup() {
        String group = "group1";
        double expectedMaxPrice = 15.05;
        assertEquals(expectedMaxPrice, itemService.getMaxPrice(group), 0.1);
    }

    @Test
    public void MaxPriceCouldNotBeGotByGroupIfNotExist() {
        String group = "notExist";
        double expectedMaxPrice = 0;
        assertEquals(expectedMaxPrice, itemService.getMaxPrice(group), 0.1);
        assertEquals(expectedMaxPrice, itemService.getMaxPrice(null), 0.1);
    }

    @Test
    public void MaxPriceCouldBeGotBySubgroup() {
        String group = "group1";
        String subgroup = "subgroup1";
        double expectedMaxPrice = 13.05;
        assertEquals(expectedMaxPrice, itemService.getMaxPrice(group, subgroup), 0.1);
    }

    @Test
    public void MaxPriceCouldNotBeGotBySubgroupIfNotExist() {
        String group = "group1";
        String subgroup = "subgroup1";

        double expectedMaxPrice = 0;
        assertEquals(expectedMaxPrice, itemService.getMaxPrice("notExist", subgroup), 0.1);
        assertEquals(expectedMaxPrice, itemService.getMaxPrice(null, subgroup), 0.1);
        assertEquals(expectedMaxPrice, itemService.getMaxPrice(group, null), 0.1);
        assertEquals(expectedMaxPrice, itemService.getMaxPrice(group, "notExist"), 0.1);
    }

    @Test
    public void MaxPriceCouldBeGotBySearchSequence() {
        String search = "SeArChTesT";
        double expectedMaxPrice = 14.05;
        assertEquals(expectedMaxPrice, itemService.getMaxPriceOfSearchItems(search), 0.1);
    }

    @Test
    public void MaxPriceCouldNotBeGotBySearchSequenceIfNotExist() {
        String search = "notExist";
        double expectedMaxPrice = 0;
        assertEquals(expectedMaxPrice, itemService.getMaxPriceOfSearchItems(search), 0.1);
        assertEquals(expectedMaxPrice, itemService.getMaxPriceOfSearchItems(null), 0.1);
    }

    @Test
    public void AllGroupNamesCouldBeGot() {
        List<String> groups = itemService.getGroups();
        assertSame(3, groups.size());
        assertTrue(groups.get(0).compareTo(groups.get(1)) <= -1);
        assertTrue(groups.get(1).compareTo(groups.get(2)) <= -1);
    }

    @Test
    public void AllSubgroupNamesCouldBeGotByGroup() {
        String group = "group1";

        List<String> subgroups = itemService.getSubgroups(group);
        assertSame(2, subgroups.size());
        assertTrue(subgroups.get(0).compareTo(subgroups.get(1)) <= -1);
    }

    @Test
    public void SubgroupNamesCouldNotBeGotByGroupIfNotExist() {
        String group = "notExist";
        assertSame(0, itemService.getSubgroups(group).size());
        assertSame(0, itemService.getSubgroups(null).size());
    }

    @Test
    public void FeedBacksCouldBeGotByItemId() {
        long id = 101L;
        assertSame(3, itemService.getFeedBacks(id).size());
    }

    @Test
    public void FeedBacksCouldNotBeGotByItemIdIfNullOrNotExistInDB() {
        long id = 10L;
        assertSame(0, itemService.getFeedBacks(id).size());
        assertSame(0, itemService.getFeedBacks(null).size());
    }


    @Test
    public void FeedBacksCouldBeSaveByItemId() {
        long id = 101L;
        int sizeBeforeSaving = itemService.getFeedBacks(id).size();
        assertTrue(itemService.saveFeedBack(id, new FeedBack("author", "comment", 0)));
        int sizeAfterSaving = itemService.getFeedBacks(id).size();
        assertSame(sizeBeforeSaving + 1, sizeAfterSaving);
    }

    @Test
    public void FeedBacksCouldNotBeSaveByItemIdIfNullOrNotExistInDBOrFeedBackIsNull() {
        long id = 101L;
        int sizeBeforeSaving = itemService.getFeedBacks(id).size();
        assertFalse(itemService.saveFeedBack(id, null));
        int sizeAfterSaving = itemService.getFeedBacks(id).size();
        assertSame(sizeBeforeSaving, sizeAfterSaving);

        assertFalse(itemService.saveFeedBack(null, new FeedBack("author", "comment", 0)));
        assertFalse(itemService.saveFeedBack(10L, new FeedBack("author", "comment", 0)));
    }
}
