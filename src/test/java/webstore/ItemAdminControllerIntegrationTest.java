package webstore;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import webstore.item.Item;
import webstore.item.controllers.ItemAdminController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testDB")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:testBefore.sql")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ItemAdminControllerIntegrationTest {

    @Autowired
    private ItemAdminController itemAdminController;
    @Autowired
    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(itemAdminController).build();
    }

    @Test
    public void itemCouldBeSave() throws Exception {
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);

        mockMvc.perform(put("/api/v.0.1/admin/item")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(mapper.writeValueAsString(item))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk());
    }

    @Test
    public void itemCouldBeUpdate() throws Exception {
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(101L);
        mockMvc.perform(put("/api/v.0.1/admin/item")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(mapper.writeValueAsString(item))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk());
    }

    @Test
    public void itemCouldNotBeSaveOrUpdateIfNotValid() throws Exception {
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setGroupName("");
        mockMvc.perform(put("/api/v.0.1/admin/item")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(mapper.writeValueAsString(item))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void itemCouldNotBeUpdateIfNotExistInDB() throws Exception {
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        mockMvc.perform(put("/api/v.0.1/admin/item")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(mapper.writeValueAsString(item))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void itemCouldBeDelete() throws Exception {
        mockMvc.perform(delete("/api/v.0.1/admin/item/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk());
    }

    @Test
    public void itemCouldNotBeDeleteIfNotExistInDB() throws Exception {
        mockMvc.perform(delete("/api/v.0.1/admin/item/10")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNotFound());
    }

}
