package webstore;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import webstore.item.Item;
import webstore.item.controllers.ItemLoadController;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testDB")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:testBefore.sql")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ItemLoadControllerIntegrationTest {

    @Autowired
    private ItemLoadController itemLoadController;
    @Autowired
    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(itemLoadController).build();
    }

    @Test
    public void itemCouldBeReadIfExist() throws Exception {
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(101L);
        Set<Item> likes = new HashSet<>();
        likes.add(item);
        Map<Item, Integer> basket = new HashMap<>();
        basket.put(item, 1);


        MvcResult result = mockMvc.perform(get("/api/v.0.1/item/101")
                .sessionAttr("likes", likes).sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();
        String itemJson = result.getResponse().getContentAsString();
        item = mapper.readValue(itemJson, Item.class);
        assertTrue(item.isInBasket());
        assertTrue(item.isLike());

        result = mockMvc.perform(get("/api/v.0.1/item/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();
        itemJson = result.getResponse().getContentAsString();
        item = mapper.readValue(itemJson, Item.class);
        assertFalse(item.isInBasket());
        assertFalse(item.isLike());


        result = mockMvc.perform(get("/api/v.0.1/item/102")
                .sessionAttr("likes", likes).sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();
        itemJson = result.getResponse().getContentAsString();
        item = mapper.readValue(itemJson, Item.class);
        assertFalse(item.isInBasket());
        assertFalse(item.isLike());



    }

    @Test
    public void itemCouldNotBeReadIfNotExist() throws Exception {
        mockMvc.perform(get("/api/v.0.1/item/11")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    @Test
    public void groupNamesCouldNotBeRead() throws Exception {
        mockMvc.perform(get("/api/v.0.1/groupNames")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
    }

    @Test
    public void subgroupNamesCouldBeReadIfGroupExist() throws Exception {
        mockMvc.perform(get("/api/v.0.1/subgroupNames/group1")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
    }

    @Test
    public void subgroupNamesCouldNotBeReadIfGroupNotExist() throws Exception {
        mockMvc.perform(get("/api/v.0.1/subgroupNames/notExist")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    @Test
    public void countOfPageOfLoadingItemsCouldBeRead() throws Exception {
        mockMvc.perform(get("/api/v.0.1/pages")
                .sessionAttr("pages", 4)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().json("4"));

        mockMvc.perform(get("/api/v.0.1/pages")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().json("0"));
    }

    @Test
    public void maxPriceOfLoadingItemsCouldBeRead() throws Exception {
        mockMvc.perform(get("/api/v.0.1/max-price")
                .sessionAttr("max-price", 100.0)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().json("100.0"));

        mockMvc.perform(get("/api/v.0.1/max-price")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().json("0.0"));
    }

    /**
     * Page is not required
     * @throws Exception
     */
    @Test
    public void itemsCouldBeReadByGroup() throws Exception {
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(110L);
        Set<Item> likes = new HashSet<>();
        likes.add(item);
        Map<Item, Integer> basket = new HashMap<>();
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(get("/api/v.0.1/items/group1")
                .sessionAttr("likes", likes).sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        double maxPrice = (double) result.getRequest().getSession().getAttribute("max-price");
        assertEquals(15.05, maxPrice, 0.1);

        int pages = (int) result.getRequest().getSession().getAttribute("pages");
        assertSame(3, pages);

        String itemsJson = result.getResponse().getContentAsString();
        Item[] items = mapper.readValue(itemsJson, Item[].class);

        for (Item i : items) {
            if (i.getId().equals(110L)) {
                assertTrue(i.isInBasket());
                assertTrue(i.isLike());
            }
        }

        result = mockMvc.perform(get("/api/v.0.1/items/group1?page=0")
                .sessionAttr("likes", likes).sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJsonPage = result.getResponse().getContentAsString();

        assertEquals(itemsJson, itemsJsonPage);
    }

    @Test
    public void itemsCouldNotBeReadByGroupIfNotExist() throws Exception {
        mockMvc.perform(get("/api/v.0.1/items/notExist")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    /**
     * Page is not required
     * @throws Exception
     */
    @Test
    public void itemsCouldBeReadBySubgroup() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v.0.1/items/group1/subgroup1")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        double maxPrice = (double) result.getRequest().getSession().getAttribute("max-price");
        assertEquals(13.05, maxPrice, 0.1);

        String itemsJson = result.getResponse().getContentAsString();

        result = mockMvc.perform(get("/api/v.0.1/items/group1/subgroup1?page=0")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJsonPage = result.getResponse().getContentAsString();

        assertEquals(itemsJson, itemsJsonPage);
    }

    @Test
    public void itemsCouldNotBeReadBySubgroupIfNotExist() throws Exception {
        mockMvc.perform(get("/api/v.0.1/items/group1/notExist")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/v.0.1/items/notExist/subgroup1")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/v.0.1/items/notExist/notExist")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }


    /**
     * Page is not required
     * @throws Exception
     */
    @Test
    public void itemsCouldBeReadBySearchSequenceIgnoreCase() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v.0.1/items/search/SeArChTeSt")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        double maxPrice = (double) result.getRequest().getSession().getAttribute("max-price");
        assertEquals(14.05, maxPrice, 0.1);

        String itemsJson = result.getResponse().getContentAsString();

        result = mockMvc.perform(get("/api/v.0.1/items/search/SeArChTeSt?page=0")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJsonPage = result.getResponse().getContentAsString();

        assertEquals(itemsJson, itemsJsonPage);
    }

    @Test
    public void itemsCouldNotBeReadBySearchSequenceIfNotExist() throws Exception {
        mockMvc.perform(get("/api/v.0.1/items/search/notExist")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    /**
     * None filter is required
     * Page is not required
     * @throws Exception
     */
    @Test
    public void itemsCouldBeReadByGroupAndFilter() throws Exception {
        mockMvc.perform(get("/api/v.0.1/items/group1/filter")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));

        mockMvc.perform(get("/api/v.0.1/items/group1/filter?sort=title-DESC")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));

        mockMvc.perform(get("/api/v.0.1/items/group1/filter?price=0-10000")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));

        mockMvc.perform(get("/api/v.0.1/items/group1/filter?price=0-10000&sort=title-DESC")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));

        mockMvc.perform(get("/api/v.0.1/items/group1/filter?price=notExist&sort=notExist")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));

        mockMvc.perform(get("/api/v.0.1/items/group1/filter?price=0-10000&sort=title-DESC&page=1")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));
    }

    /**
     * None filter is required
     * Page is not required
     * @throws Exception
     */
    @Test
    public void itemsCouldBeReadBySubgroupAndFilter() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v.0.1/items/group1/subgroup1/filter?price=0-10000&sort=title-DESC&page=0")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJson = result.getResponse().getContentAsString();

        result = mockMvc.perform(get("/api/v.0.1/items/group1/subgroup1/filter?price=0-10000&sort=title-DESC")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJsonPage = result.getResponse().getContentAsString();

        assertEquals(itemsJson, itemsJsonPage);

        mockMvc.perform(get("/api/v.0.1/items/group1/subgroup1/filter?price=0-10000&sort=title-DESC&page=1")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    /**
     * None filter is required
     * Page is not required
     * @throws Exception
     */
    @Test
    public void itemsCouldBeReadBySearchSequenceIgnoreCaseAndFilter() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v.0.1/items/search/SeArChTeSt/filter?price=0-10000&sort=title-DESC")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJson = result.getResponse().getContentAsString();

        result = mockMvc.perform(get("/api/v.0.1/items/search/SeArChTeSt/filter?price=0-10000&sort=title-DESC&page=0")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJsonPage = result.getResponse().getContentAsString();

        assertEquals(itemsJson, itemsJsonPage);
    }
}