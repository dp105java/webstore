package webstore;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import webstore.item.Filter;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest()
public class FilterParsingTest {

    @Autowired
    private Filter filter;

    @Test
    public void parsingSortTest() {
        List<String> validSort = new ArrayList<>();
        validSort.add("title-DESC");
        validSort.add("title-ASC");
        validSort.add("averageGrade-DESC");
        validSort.add("averageGrade-ASC");
        validSort.add("price-DESC");
        validSort.add("price-ASC");
        validSort.add("DESC-title");
        validSort.add("ASC-title");
        validSort.add("DESC-averageGrade");
        validSort.add("ASC-averageGrade");
        validSort.add("DESC-price");
        validSort.add("ASC-price");

        List<String> notValidSort = new ArrayList<>();
        notValidSort.add(null);
        notValidSort.add("titleDESC");
        notValidSort.add("title-ASC-price");
        notValidSort.add("averageGrade1-DESC");
        notValidSort.add("averageGrade-ASC2");

        for (String sort : validSort) {
            filter.parseSort(sort);
            assertNotNull(filter.getSortObj());
        }

        for (String sort : notValidSort) {
            filter.parseSort(sort);
            assertNull(filter.getSortObj());
        }
    }

    @Test
    public void parsingPriceTest() {
        List<String> validPrice = new ArrayList<>();
        validPrice.add("150-200");
        validPrice.add("200-150");
        for (String p : validPrice){
            filter.parsePrice(p);
            assertEquals(150, filter.getMinPrice());
            assertEquals(200, filter.getMaxPrice());
        }

        List<String> notValidPrice = new ArrayList<>();
        notValidPrice.add(null);
        notValidPrice.add("150-200-300");
        notValidPrice.add("1500");
        notValidPrice.add("15sds0-200");
        notValidPrice.add("150-20fdgdfg0");
        notValidPrice.add("-150-200");
        notValidPrice.add("-150");
        notValidPrice.add("150-");
        notValidPrice.add("-");

        for (String p : notValidPrice){
            filter.parsePrice(p);
            assertEquals(0, filter.getMinPrice());
            assertEquals(Integer.MAX_VALUE, filter.getMaxPrice());
        }
    }

}
