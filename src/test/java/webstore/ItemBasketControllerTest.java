package webstore;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import webstore.item.Item;
import webstore.item.controllers.ItemBasketController;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = "spring.datasource.url=jdbc:postgresql://localhost:5432/testDB")
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:testBefore.sql")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ItemBasketControllerTest {
    @Autowired
    private ItemBasketController itemBasketController;
    @Autowired
    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(itemBasketController).build();
    }

    @Test
    public void itemCouldBeAddToBasket() throws Exception {
        MvcResult result = mockMvc.perform(post("/api/v.0.1/basket/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        Map<Item, Integer> basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(1, basket.size());
        Item item = basket.keySet().stream().findFirst().get();
        assertSame(101L, item.getId());
        assertSame(1, basket.get(item));
    }

    @Test
    public void itemCouldBeAddToBasketWhenBasketNotEmpty() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(post("/api/v.0.1/basket/101")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(2, basket.size());
    }

    @Test
    public void itemCouldNotBeAddToBasketIfItemNotExist() throws Exception {
        mockMvc.perform(post("/api/v.0.1/basket/10")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    @Test
    public void itemCouldBeRemoveFromBasket() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(101L);
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(delete("/api/v.0.1/basket/101")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(0, basket.size());
    }

    @Test
    public void itemCouldNotBeRemoveFromBasketIfNotExistBasket() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(delete("/api/v.0.1/basket/101")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent())
                .andReturn();

        basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(1, basket.size());
    }

    @Test
    public void itemCouldNotBeRemoveFromBasketIfBasketIsNullOrEmpty() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        mockMvc.perform(delete("/api/v.0.1/basket/101")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());

        mockMvc.perform(delete("/api/v.0.1/basket/101")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());
    }

    @Test
    public void BasketCouldBeGet() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(get("/api/v.0.1/basket")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();


        String itemsJson = result.getResponse().getContentAsString();
        Map<String, Integer> basketJson = mapper.readValue(itemsJson, HashMap.class);
        assertSame(1, basketJson.size());

        String itemJson = basketJson.keySet().stream().findFirst().get();
        Item i = mapper.readValue(itemJson , Item.class);
        assertSame(10L, i.getId());
        assertSame(1, basketJson.get(itemJson));
    }

    @Test
    public void basketCouldBeGetIfBasketIsNull() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v.0.1/basket")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andReturn();

        String itemsJson = result.getResponse().getContentAsString();
        Map<String, Integer> basketJson = mapper.readValue(itemsJson, HashMap.class);
        assertSame(0, basketJson.size());
    }

    @Test
    public void basketCouldBeRemoved() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(10L);
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(delete("/api/v.0.1/basket")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(0, basket.size());
    }

    @Test
    public void countOfItemInBasketCouldBeChange() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(101L);
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(post("/api/v.0.1/basket/changeCount/101/1")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(1, basket.size());
        item = basket.keySet().stream().findFirst().get();
        assertSame(101L, item.getId());;
        assertSame(2, basket.get(item));

        result = mockMvc.perform(post("/api/v.0.1/basket/changeCount/101/-1")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(1, basket.size());
        item = basket.keySet().stream().findFirst().get();
        assertSame(101L, item.getId());;
        assertSame(1, basket.get(item));
    }

    @Test
    public void countOfItemInBasketCouldNotBeChangeIfNewCountLessThen1() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(101L);
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(post("/api/v.0.1/basket/changeCount/101/-1")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andReturn();

        basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(1, basket.size());
        item = basket.keySet().stream().findFirst().get();
        assertSame(101L, item.getId());;
        assertSame(1, basket.get(item));
    }

    @Test
    public void countOfItemInBasketCouldNotBeChangeIfItemNotExistInBasket() throws Exception {
        Map<Item, Integer> basket = new HashMap<>();
        Item item = new Item("GR", "SG", "T", "D", 5.5, 4);
        item.setId(101L);
        basket.put(item, 1);

        MvcResult result = mockMvc.perform(post("/api/v.0.1/basket/changeCount/10/1")
                .sessionAttr("basket", basket)
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent())
                .andReturn();

        basket = (Map<Item, Integer>) result.getRequest().getSession().getAttribute("basket");
        assertSame(1, basket.size());
        item = basket.keySet().stream().findFirst().get();
        assertSame(101L, item.getId());;
        assertSame(1, basket.get(item));
    }

    @Test
    public void countOfItemInBasketCouldNotBeChangeIfBasketNull() throws Exception {
        mockMvc.perform(post("/api/v.0.1/basket/changeCount/10/1")
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNoContent());

    }

}
