var total;
var basketItems;

function addToBasket(itemId) {
    console.log('adding to basket item #' + itemId);
    $.ajax({
        method: 'POST',
        url: '/api/v.0.1/basket/' + itemId,
        success: function () {
            console.log('success adding item to basket');
            getAllItemsFromBasket();

            var id = '#basket-' + itemId;
            $(id).html('<img src="images/icon-basket-full.png" alt="basket" onclick=openBasket(' + itemId + ')>');
        }
    });
}

function removeFromBasket(itemId) {
    console.log('removing from basket item #' + itemId);
    $.ajax({
        method: 'DELETE',
        url: '/api/v.0.1/basket/' + itemId,
        success: function () {
            console.log('success removing item from basket');
            getAllItemsFromBasket();

            var id = '#basket-' + itemId;
            $(id).html('<img src="images/icon-basket-empty.png" alt="basket" onclick=addToBasket(' + itemId + ')>');
        }
    });
}

function getAllItemsFromBasket() {
    console.log('getting items from basket');
    $.ajax({
        method: 'GET',
        url: 'api/v.0.1/basket',
        success: function (items) {
            basketItems = items;

            if (basketItems == undefined) {
                $('#basket').html(0);
            } else {
                $('#basket').html(Object.keys(basketItems).length);
            }

            displayItemsFromBasket();
        }
    })
}

function removeAllItemsFromBasket() {
    console.log('getting likeList');
    $.ajax({
        method: 'DELETE',
        url: 'api/v.0.1/basket',
        success: function () {
            $('#basket').html(0);
            
            $.each(basketItems, function (it, count) {
                var item = JSON.parse(it);
                var id = '#basket-' + item.id;
                $(id).html('<img src="images/icon-basket-empty.png" alt="basket" onclick=addToBasket(' + item.id + ')>');
            });

            basketItems = [];
            displayItemsFromBasket();
        }
    })
}

function displayItemsFromBasket() {
    console.log('displaying all items from basket');
    var html = '';
    total = 0;
    $.each(basketItems, function (it, count) {
        var item = JSON.parse(it);
        total += item.price * count;
        html +=
            '<div class="row">' +
            '<a href="javascript:void(0);" onclick="loadItemById(' + item.id + ')">' +
            '<div class="col-md-2 liked-item-img">' +
            '<img src="' + item.imgPath + '" >' +
            '</div>' +
            '<div class="col-md-6"><h4>' + item.title + '</h4></div>' +
            '</a>' +
            '<div class="col-md-1"><h4>' + item.price + '</h4></div>' +
            '<div class="col-md-2">' +
            '<div class="input-group input-group-sm">' +
            '<span class="input-group-btn">' +
            '<button class="btn btn-default" type="button" onclick="decrementItemInBasket(' + item.id + ', ' + item.price + ')">' +
            '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>' +
            '</button>' +
            '</span>' +
            '<input type="text" id="count-' + item.id + '" class="form-control text-center" readonly value="' + count + '">' +
            '<span class="input-group-btn">' +
            '<button class="btn btn-default" type="button" onclick="incrementItemInBasket(' + item.id + ', ' + item.price + ')">' +
            '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
            '</button>' +
            '</span>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-1 icon-remove">' +
            '<a href="javascript:void(0);" onclick="removeFromBasket(' + item.id + ')">' +
            '<img src="images/icon-close.png">' +
            '</a>' +
            '</div>' +
            '</div>';

    });

    html +=
        '<hr><div class="row"><div id="total" class="col-md-11 text-right"><h3>' + total + '</h3></div></div>';

    $('#basket-items').html(html);

}

function incrementItemInBasket(itemId, price) {
    var id = '#count-' + itemId;
    var count = $(id).val();
    count++;
    total += price;

    $(id).val(count);
    $('#total').html('<h3>' + total + '</h3>');

    changeCountOfItem(itemId, 1);
}

function decrementItemInBasket(itemId, price) {
    var id = '#count-' + itemId;
    var count = $(id).val();
    if (count >= 2) {
        count--;
        total -= price;
        $(id).val(count);
        $('#total').html('<h3>' + total + '</h3>');

        changeCountOfItem(itemId, -1);
    }
}

function changeCountOfItem(itemId, delta) {
    $.ajax({
        method: 'POST',
        url: '/api/v.0.1/basket/changeCount/' + itemId + '/' + delta,
        success: function () {
            console.log('success changing count of intems on ' + delta);
            getAllItemsFromBasket();
        }
    });
}

function openBasket() {
    getAllItemsFromBasket();
    $('#basketModal').modal();
}