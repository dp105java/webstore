$(document).ready(function () {
    $('#newItem').submit(function(e) {

        var data = {
            id:             $('#itemId').val(),
            groupName:      $('#groupName').val(),
            subgroupName:   $('#subgroupName').val(),
            title:          $('#title').val(),
            description:    $('#description').val(),
            price:          $('#price').val(),
            imgPath:        $('#imgPath').val()
        };

        jsonData = JSON.stringify(data);

        console.log('formData: ' + jsonData);

        e.preventDefault();

        $.ajax({
            type: "PUT",
            url: "/api/v.0.1/admin/item",
            data: jsonData,
            statusCode : {
                200 : function() {
                    console.log('item was update successful');
                    var itemId = $('#itemId').val();
                    $('#itemModal').modal('hide');
                    loadItemById(itemId);
                },
                201 : function() {
                    console.log('item was create successful');
                    $('#itemModal').modal('hide');
                },
                400 : function() {
                    alert('cant validate item. check field');
                    $('#itemModal').modal('show');
                }
            },
            dataType: "json",
            contentType : "application/json"
        });
    });
});

function createItem() {
    $('#itemId').val('');
    $('#groupName').val('');
    $('#subgroupName').val('');
    $('#title').val('');
    $('#description').val('');
    $('#price').val('');
    $('#imgPath').val('');
    $('#itemModal').modal();
}

function updateItem(itemId) {
    console.log('updating item');
    $.ajax({
        method: 'GET',
        url: '/api/v.0.1/item/' + itemId,
        success: function (item) {
            console.log('success loading item ' + itemId );
            $('#itemId').val(item.id);
            $('#groupName').val(item.groupName);
            $('#subgroupName').val(item.subgroupName);
            $('#title').val(item.title);
            $('#description').val(item.description);
            $('#price').val(item.price);
            $('#imgPath').val(item.imgPath);
            $('#itemModal').modal();
        }
    });
}

function deleteItem(itemId) {
    $.ajax({
        method: 'DELETE',
        url: '/api/v.0.1/admin/item/' + itemId,
        success: function () {
            console.log('success deltitng item ' + itemId );
            loadItems(0);
        }
    });
}
