function displayItems(items, page) {
    
    var html = "";
    $.each(items, function (i) {
        var grade = items[i].averageGrade;

        var stars = '';
        for (var n = 0; n < 5; n++, grade--) {
            if (grade >= 1) {
                stars += '<img src="images/icon-star-full.png" alt="full star">'
            } else if (grade > 0) {
                stars += '<img src="images/icon-star-half.png" alt="half star">'
            } else {
                stars += '<img src="images/icon-star-empty.png" alt="half star">'
            }
        }

        var heart = '';
        if (items[i].like) {
            heart += '<img src="images/icon-heart.png" alt="heart" onclick=dislikeItem(' + items[i].id + ')>'
        } else {
            heart += '<img src="images/icon-heart-empty.png" alt="heart" onclick=likeItem(' + items[i].id + ')>'
        }

        var basket = '';
        if (items[i].inBasket) {
            basket += '<img src="images/icon-basket-full.png" alt="basket" onclick=openBasket()>'
        } else {
            basket += '<img src="images/icon-basket-empty.png" alt="basket" onclick=addToBasket(' + items[i].id + ')>'
        }

        html += '<a href="javascript:void(0);" class="col-xs-12 col-sm-12 col-md-6 col-lg-4" >' +
            '<div class="thumbnail all_items">' +
            '<img src="' + items[i].imgPath + '" alt="img" onclick=loadItemById(' + items[i].id + ')>' +
            '<div class="caption" onclick=loadItemById(' + items[i].id + ')>' +
            '<h4>' + items[i].title + '</h4>' +
            '<h3>' + items[i].price + '</h3>' +
            '</div>' +
            '<div class="row icon-container" >' +
            '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 icon-star">' + stars + '</div>' +
            '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 icon-heart" id="heart-' + items[i].id + '">' + heart + '</div>' +
            '<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 icon-basket" id="basket-' + items[i].id + '">' + basket + '</div>' +
            '</div>' +
            '</div>' +
            '</a>';

    });
    
    if (page == 0){
        $('#items').html(html);
    } else {
        $('#items').append(html);
    }

}

function displayItem(item) {
    console.log('displaying item');
    var html = "";

    var grade = item.averageGrade;
    var stars = '';
    for (var n = 0; n < 5; n++, grade--) {
        if (grade >= 1) {
            stars += '<img src="images/icon-star-full.png" alt="full star">'
        } else if (grade > 0) {
            stars += '<img src="images/icon-star-half.png" alt="half star">'
        } else {
            stars += '<img src="images/icon-star-empty.png" alt="half star">'
        }
    }

    var heart = '';
    if (item.like) {
        heart += '<img src="images/icon-heart.png" alt="heart" onclick=dislikeItem(' + item.id + ')>'
    } else {
        heart += '<img src="images/icon-heart-empty.png" alt="heart" onclick=likeItem(' + item.id + ')>'
    }

    var basket = '';
    if (item.inBasket) {
        basket += '<img src="images/icon-basket-full.png" alt="basket" onclick=openBasket()>'
    } else {
        basket += '<img src="images/icon-basket-empty.png" alt="basket" onclick=addToBasket(' + item.id + ')>'
    }

    html +=
        '<div class="thumbnail one_item">' +
        '<img src="' + item.imgPath + '" alt="img">' +
        '<div class="caption">' +
        '<h4>' + item.title + '</h4>' +
        '<h4>' + item.price + '</h4>' +
        '<div class="row icon-container" >' +
        '<div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 icon-star">' + stars + '</div>' +
        '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 icon-heart" id="heart-' + item.id + '">' + heart + '</div>' +
        '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 icon-basket" id="basket-' + item.id + '">' + basket + '</div>' +
        '</div>' +
        '<ul class="nav nav-tabs">' +
        '<li id="desc" class="active"><a href="javascript:void(0);" onclick=displayDescriptions(' + item.id + ')>Описание</a></li>' +
        '<li id="feedBacks" ><a href="javascript:void(0);" onclick=displayFeedBacks(' + item.id + ')>Отзывы</a></li>' +
        '<li id="leftFeedBacks" ><a href="javascript:void(0);" onclick=leaveFeedBack(' + item.id + ')>Оставить отзыв</a></li>' +
        '<li><a href="javascript:void(0);" onclick=updateItem(' + item.id + ')>Изменить</a></li>' +
        '<li><a href="javascript:void(0);" onclick=deleteItem(' + item.id + ')>Удалить</a></li>' +
        '</ul> ' +
        '</div>' +
        '<div id="desc-container"><textarea readonly class="desc-area">' + item.description + '</textarea></div>' +
        '</div>';


    $('#items').html(html);
    $('#slider-container').html("");
    $('#sorts').html("");
}

