var likedItems;

function likeItem(itemId) {
    console.log('sending to likeList item #' + itemId);
    $.ajax({
        method: 'POST',
        url: '/api/v.0.1/like/' + itemId,
        success: function () {
            console.log('success sending item to likeList');
            getLikedItems();
            
            var id = '#heart-' + itemId;
            $(id).html('<img src="images/icon-heart.png" alt="heart" onclick=dislikeItem(' + itemId + ')>');
        }
    });
}

function dislikeItem(itemId) {
    console.log('sending to remove from likeList item #' + itemId);
    $.ajax({
        method: 'DELETE',
        url: '/api/v.0.1/dislike/' + itemId,
        success: function () {
            console.log('success removing item from likeList');
            getLikedItems();

            var id = '#heart-' + itemId;
            $(id).html('<img src="images/icon-heart-empty.png" alt="heart" onclick=likeItem(' + itemId + ')>');
        }
    });
}

function getLikedItems() {
    console.log('getting likeList');
    $.ajax({
        method: 'GET',
        url: 'api/v.0.1/likes',
        success: function (items) {
            
            likedItems = items;

            if (likedItems == undefined) {
                $('#likes').html(0);
            } else {
                $('#likes').html(likedItems.length);
                displayLikedItems();
            }

        }
    })
}

function removeLikedItems() {
    console.log('getting likeList');
    $.ajax({
        method: 'DELETE',
        url: 'api/v.0.1/likes',
        success: function () {
            $('#likes').html(0);
            $.each(likedItems, function (i) {
                var id = '#heart-' + likedItems[i].id;
                $(id).html('<img src="images/icon-heart-empty.png" alt="heart" onclick=likeItem(' + likedItems[i].id + ')>');

            });
            likedItems = [];
            displayLikedItems();
        }
    })
}

function displayLikedItems() {
    var html = '';
    $.each(likedItems, function (i) {
        html +=
            '<div class="row">' +
            '<a href="javascript:void(0);" onclick="loadItemById(' + likedItems[i].id + ')">' +
            '<div class="col-md-2 liked-item-img">' +
            '<img src="' + likedItems[i].imgPath + '" >' +
            '</div>' +
            '<div class="col-md-7"><h4>' + likedItems[i].title + '</h4></div>' +
            '</a>' +
            '<div class="col-md-2"><h4>' + likedItems[i].price + '</h4></div>' +
            '<div class="col-md-1 icon-remove">' +
            '<a href="javascript:void(0);" onclick="dislikeItem(' + likedItems[i].id + ')">' +
            '<img src="images/icon-close.png">' +
            '</a>' +
            '</div>' +
            '</div>';
    });
    $('#liked-items').html(html);
}
