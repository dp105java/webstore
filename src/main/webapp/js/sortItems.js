function addPriceRanger() {
    console.log('add price range');

    var min = 0;
    var max = maxPrice;

    var html =
        '<div id="price-container">' +
        '<input type="text" class="form-control input-sm" id="minPrice" value="' + min + '" placeholder="' + min + '"/>' +
        '<input type="text" class="form-control input-sm" id="maxPrice" value="' + max + '" placeholder="' + max + '"/>' +
        '<button class="btn btn-default btn-sm" onclick="rangeItems()">Load</button>' +
        '</div>' +
        '<br><br>' +

        '<div id="slider"></div>';

    $('#slider-container').html(html);

    $("#slider").slider({
        min: 0,
        max: max,
        values: [min, max],
        range: true,
        stop: function (event, ui) {
            $("#minPrice").val($("#slider").slider("values", 0));
            $("#maxPrice").val($("#slider").slider("values", 1));
        },
        slide: function (event, ui) {
            $("#minPrice").val($("#slider").slider("values", 0));
            $("#maxPrice").val($("#slider").slider("values", 1));
        }
    });

    $("#minPrice").change(function () {
        var value1 = $("#minPrice").val();
        var value2 = $("#maxPrice").val();

        if (parseInt(value1) > parseInt(value2)) {
            value1 = value2;
            $("#minPrice").val(value1);
        }
        $("#slider").slider("values", 0, value1);
    });

    $("#maxPrice").change(function () {
        var value1 = $("#minPrice").val();
        var value2 = $("#maxPrice").val();

        if (parseInt(value1) > parseInt(value2)) {
            value2 = value1;
            $("#maxPrice").val(value2)
        }
        if (parseInt(value2) > max) {
            value2 = max;
            $("#maxPrice").val(value2)
        }

        $("#slider").slider("values", 1, value2);
    });
}

function addSorts() {
    console.log('add sorts');
    var html =
        '<a class="list-group-item active" href="#"> Сортировать</a>' +
        '<a class="list-group-item" href="#" onclick=sortItems("ASC","title")>' +
        '<span class="glyphicon glyphicon glyphicon-arrow-up" aria-hidden="true"></span> Название</a>' +
        '<a class="list-group-item" href="#" onclick=sortItems("DESC","title")>' +
        '<span class="glyphicon glyphicon glyphicon-arrow-down" aria-hidden="true"></span> Название</a>' +
        '<a class="list-group-item" href="#" onclick=sortItems("ASC","price")>' +
        '<span class="glyphicon glyphicon glyphicon-arrow-up" aria-hidden="true"></span> Цена</a>' +
        '<a class="list-group-item" href="#" onclick=sortItems("DESC","price")>' +
        '<span class="glyphicon glyphicon glyphicon-arrow-down" aria-hidden="true"></span> Цена</a>' +
        '<a class="list-group-item" href="#" onclick=sortItems("ASC","averageGrade")>' +
        '<span class="glyphicon glyphicon glyphicon-arrow-up" aria-hidden="true"></span> Оценка</a>' +
        '<a class="list-group-item" href="#" onclick=sortItems("DESC","averageGrade")>' +
        '<span class="glyphicon glyphicon glyphicon-arrow-down" aria-hidden="true"></span> Оценка</a>';
    $('#sorts').html(html);
}

function sortItems (direction, property) {
    sort = property + '-' + direction;
    filter = true;
    loadItems(0);
}

function rangeItems() {
    var min = $("#minPrice").val();
    var max = $("#maxPrice").val();
    price = min + '-' + max;
    filter = true;
    loadItems(0);
}

