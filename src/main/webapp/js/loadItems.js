$(document).ready(function () {
    likedItems = [];
    basketItems = [];
    mainCatalog = '';
    total = 0;
    
    defaultSort = 'title-ASC';
    sort = defaultSort;
    filter = false;
    
    loadMainPage();
    getLikedItems();
    getAllItemsFromBasket();
});

var sort;
var defaultSort;
var price;
var maxPrice;
var minPrice;
var currentUrl;
var currentPage;
var mainCatalog;
var filter;


function loadMainPage() {
    console.log('loading main page');
    $.ajax({
        method: 'GET',
        url: '/api/v.0.1/groupNames',
        success: function (groups) {
            mainCatalog = '';
            var items = '';
            var catalog = '<div class="list-group-item active" href="#">Каталог товаров</div>';

            $.each(groups, function (i) {
                mainCatalog += '<li><a href="#" onclick=loadSubgroups("' + groups[i] + '")>' + groups[i] + '</a></li>';
                catalog += '<a class="list-group-item" href="#" onclick=loadSubgroups("' + groups[i] + '")>' + groups[i] + '</a>';
                items = items + '<a href="#" class="col-md-4" onclick=loadSubgroups("' + groups[i] + '")>' +
                    '<div class="thumbnail main-icon">' +
                    '<img src="images/' + groups[i] + '.png" alt="img" class = "main-icon">' +
                    '<div class="caption">' +
                    '<h4>' + groups[i] + '</h4>' +
                    '</div>' +
                    '</div>' +
                    '</a>';
            });
            $('.main-catalog').html(mainCatalog);
            $('#catalog').html(catalog);
            $('#items').html(items);
        }
    });
}

function loadSubgroups(group) {
    console.log('loading subgroups');
    $.ajax({
        method: 'GET',
        url: '/api/v.0.1/subgroupNames/' + group,
        success: function (subgroups) {
            var catalog = '';

            catalog += '<div class="dropdown">' +
                '<a href="javascript:void(0);" class="dropdown-toggle list-group-item active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>&nbsp Каталог &nbsp товаров <span class="caret"></span></a>' +
                '<ul class="dropdown-menu main-catalog">' + mainCatalog + '</ul>' +
                '</div>';


            catalog += '<a class="list-group-item" href="#" onclick=loadItemsByGroup("' + group + '")>' + group + ' - все</a>';
            $.each(subgroups, function (i) {
                catalog += '<a class="list-group-item" href="#" onclick=loadItemsBySubgroup("' + group + '","' + subgroups[i] + '")>' + subgroups[i] + '</a>'
            });
            $('#catalog').html(catalog);
            loadItemsByGroup(group);
        }
    });
}

function loadItemsByGroup(group) {
    console.log('loading items by group');
    currentUrl = '/api/v.0.1/items/' + group;
    turnOffFilter();
    loadItems(0);
}

function loadItemsBySubgroup(group, subgroup) {
    console.log('loading items by subgroup');
    currentUrl = '/api/v.0.1/items/' + group + '/' + subgroup;
    turnOffFilter();
    loadItems(0);
}

function searchItems() {
    var seq = $('#search').val();
    if (seq != ''){
        console.log('searching items');
        currentUrl = '/api/v.0.1/items/search/' + seq;
        turnOffFilter();
        loadItems(0);
    }
}

function turnOffFilter() {
    filter = false;
    sort = defaultSort;
}

function loadItems(page){
    var url;
    if (filter){
        console.log('Sorting items with filter. page: ' + page);
        url = currentUrl + '/filter?sort=' + sort + '&price='+ price + '&page=' + page;
    } else {
        console.log('loading items. page: ' + page);
        url = currentUrl + '?page=' + page;
    }
    $.ajax({
        method: 'GET',
        url: url,
        success: function (items) {
            currentPage = page;
            loadPages();
            displayItems(items, page);
            if (!filter) {
                loadPriceRange();
                addSorts();
            }
        }
    });
}

function loadMore(){
    console.log('loading more items');
    loadItems(currentPage + 1);
}

function loadItemById(itemId) {
    console.log('load item by id');
    $.ajax({
        method: 'GET',
        url: '/api/v.0.1/item/' + itemId,
        success: function (item) {
            $('#load-more').html('');
            displayItem(item)
        }
    });
}

function loadPages() {
    $.ajax({
        method: 'GET',
        url: '/api/v.0.1/pages',
        success: function (pages) {
            console.log('pages: ' + pages);

            if (currentPage < (pages - 1)){
                $('#load-more').html('<img src="images/load.png" alt="load more" onclick="loadMore()">')
            } else {
                $('#load-more').html('')
            }
        }
    });
}

function loadPriceRange() {
    $.ajax({
        method: 'GET',
        url: '/api/v.0.1/max-price',
        success: function (p) {
            maxPrice = p;
            minPrice = 0;
            price = minPrice + '-' + maxPrice;
            console.log('max price: ' + maxPrice);
            addPriceRanger();
        }
    });
}
