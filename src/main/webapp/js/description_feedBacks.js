function displayDescriptions(itemId) {
    console.log('displayDescriptions: ' + itemId);

    $.ajax({
        method: 'GET',
        url: '/api/v.0.1/item/' + itemId,
        success: function (item) {
            console.log('success loading item description: ' + itemId);
            $('#desc-container').html('<textarea readonly class="desc-area">' + item.description + '</textarea>');
        }
    });

    $('#desc').addClass('active');
    $('#feedBacks').removeClass('active');
    $('#leftFeedBacks').removeClass('active');
    
}

function displayFeedBacks(itemId) {
    console.log('displayFeedBacks: ' + itemId);

    $.ajax({
        method: 'GET',
        url: '/api/v.0.1/feedBack/' + itemId,
        success: function (feedBacks) {
            console.log('success loading feedBacks for: ' + itemId);
            var html = '';
            $.each(feedBacks, function (i) {
                var grade = feedBacks[i].grade;
                var stars = '';
                for (var n = 0; n < 5; n++, grade--) {
                    if (grade >= 1) {
                        stars += '<img src="images/icon-star-full.png" alt="full star">'
                    } else {
                        stars += '<img src="images/icon-star-empty.png" alt="half star">'
                    }
                }

                html +=
                    '<div class="panel panel-default">' +
                    '<div class="panel-heading">' +
                    '<h3 class="panel-title"> ' + feedBacks[i].author +
                    '<div class="icon-star" id="feedback-grade">' + stars + '</div></h3>' +
                    '</div>' +
                    '<div class="panel-body"> ' + feedBacks[i].comment + '</div>' +
                    '</div>';
            });

            $('#desc-container').html(html);
        }
    });


    $('#feedBacks').addClass('active');
    $('#desc').removeClass('active');
    $('#leftFeedBacks').removeClass('active');

}

var grade = 0;
function leaveFeedBack(itemId) {
    console.log('leaveFeedBack: ' + itemId);
    var html =
        '<form class="form-item" id="newFeedBack">' +
        '<div class="form-group">' +
        '<input type="text" class="form-control" id="author" placeholder="Автор" required>' +
        '</div>' +
        '<div class="row col-md-12 icon-star">' +
        '<div id="star1" class="star" onmouseover=putFullStar(1)><img src="images/icon-star-empty.png" alt="half star"></div>' +
        '<div id="star2" class="star" onmouseover=putFullStar(2)><img src="images/icon-star-empty.png" alt="half star"></div>' +
        '<div id="star3" class="star" onmouseover=putFullStar(3)><img src="images/icon-star-empty.png" alt="half star"></div>' +
        '<div id="star4" class="star" onmouseover=putFullStar(4)><img src="images/icon-star-empty.png" alt="half star"></div>' +
        '<div id="star5" class="star" onmouseover=putFullStar(5)><img src="images/icon-star-empty.png" alt="half star"></div>' +
        '<span id="grade" class="badge">0</span>' +
        '</div><br><br>' +
        '<div class="form-group">' +
        '<textarea class="form-control" id="comment" placeholder="Комментарий" required></textarea>' +
        '</div>' +
        '<button type="submit" id="submit-new-item" class="btn btn-default">Отправить</button>' +
        '</form>';

    $('#leftFeedBacks').addClass('active');
    $('#feedBacks').removeClass('active');
    $('#desc').removeClass('active');
    $('#desc-container').html(html);

    $('#newFeedBack').submit(function (e) {
        var data = {
            author: $('#author').val(),
            comment: $('#comment').val(),
            grade: grade
        };

        jsonData = JSON.stringify(data);
        console.log('formData: ' + jsonData);

        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/api/v.0.1/feedBack/' + itemId,
            data: jsonData,
            error: function (xhr, ajaxOptions, thrownError) {
                if (xhr.status == '201') {
                    console.log('feedBack was create successful');
                    loadItemById(itemId);
                }
                else if (xhr.status == '400') {
                    alert('cant validate feedBack');
                }
            },
            dataType: "json",
            contentType: "application/json"
        });

    });


}

function putFullStar(count) {
    console.log('putFullStar ' + count);
    for (var i = 1; i <= 5; i++) {
        var id = '#star' + i;
        if (i <= count) {
            $(id).html('<img src="images/icon-star-full.png" alt="half star">')
        } else {
            $(id).html('<img src="images/icon-star-empty.png" alt="half star">')
        }
    }
    $('#grade').html(count);
    grade = count;
}



