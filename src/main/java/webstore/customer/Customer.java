package webstore.customer;


public interface Customer {
    String getName();
    String getPhone();
    String getEmail();
}
