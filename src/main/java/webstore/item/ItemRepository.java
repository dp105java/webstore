package webstore.item;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {
    @Query("select distinct i.groupName from Item i")
    List<String> findGroups();

    @Query("select distinct i.subgroupName from Item i where i.groupName = ?1")
    List<String> findSubgroups(String group);

    Page<Item> findByGroupName(String group, Pageable pageable);

    Page<Item> findByGroupNameAndSubgroupName(String group, String subgroup, Pageable pageable);

    @Query("select i from Item i where i.groupName = ?1 and i.price >= ?2 and i.price <= ?3")
    Page<Item> findByFilter(String group, double min, double max, Pageable pageable);

    @Query("select i from Item i where i.groupName = ?1 and i.subgroupName = ?2 and i.price >= ?3 and i.price <= ?4")
    Page<Item> findByFilter(String group, String subgroup, double min, double max, Pageable pageable);

    @Query("select max(i.price) from Item i where i.groupName = ?1")
    Double findMaxPrice(String group);

    @Query("select max(i.price) from Item i where i.groupName = ?1 and i.subgroupName = ?2")
    Double findMaxPrice(String group, String subgroup);

    @Query("select distinct i from Item i where lower(i.groupName) like concat('%',?1,'%') " +
            "or lower(i.subgroupName) like concat('%',?1,'%') or lower(i.title) like concat('%',?1,'%')")
    Page<Item> findBySearch(String charSequence, Pageable pageable);

    @Query("select distinct i from Item i where (lower(i.groupName) like concat('%',?1,'%') " +
            "or lower(i.subgroupName) like concat('%',?1,'%') or lower(i.title) like concat('%',?1,'%')) " +
            "and i.price >= ?2 and i.price <= ?3")
    Page<Item> findBySearch(String charSequence, double minPrice, double maxPrice, Pageable pageable);

    @Query("select max(i.price) from Item i where lower(i.groupName) like concat('%',?1,'%') " +
            "or lower(i.subgroupName) like concat('%',?1,'%') or lower(i.title) like concat('%',?1,'%')")
    Double findMaxPriceBySearch(String charSequence);
}
