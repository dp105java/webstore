package webstore.item;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Embeddable
public class FeedBack {
    @NotEmpty
    private String author;
    @NotEmpty
    private String comment;
    @Min(0) @Max(5)
    private int grade;

    public FeedBack() {
    }

    public FeedBack(String author, String comment, int grade) {
        this.author = author;
        this.comment = comment;
        this.grade = grade;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
