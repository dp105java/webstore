package webstore.item.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import webstore.item.Filter;
import webstore.item.Item;
import webstore.item.ItemService;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * GET     /api/v.0.1/item/{itemId}                      - read item                        return Item
 * GET     /api/v.0.1/groupNames                         - read all GroupName               return List<String>
 * GET     /api/v.0.1/subgroupNames/{group}              - read all SubgroupName            return List<String>
 * <p>
 * GET     /api/v.0.1/pages
 * GET     /api/v.0.1/max-price
 * <p>
 * GET     /api/v.0.1/items/{group}?page=
 * GET     /api/v.0.1/items/{group}/{subgroup}?page=
 * GET     /api/v.0.1/items/search/{charSequence}?page=
 * <p>
 * GET     /api/v.0.1/items/{group}/filter?sort=Title-DESC&price=0-5000&page=1
 * GET     /api/v.0.1/items/{group}/{subgroup}/filter?sort=Title-DESC&price=0-5000&page=1
 * GET     /api/v.0.1/items/search/{charSequence}/filter?sort=Title-DESC&price=0-5000&page=1
 */

@RestController
@RequestMapping(value = "/api/v.0.1", produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemLoadController {
    private Logger logger = Logger.getLogger(ItemLoadController.class);
    @Autowired
    private ItemService itemService;

    @GetMapping(value = "/item/{itemId}")
    public ResponseEntity<Item> readItem(@PathVariable("itemId") Long itemId, HttpSession session) {
        Item item = itemService.getById(itemId);
        if (item == null) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        Set<Item> likes = (Set<Item>) session.getAttribute("likes");
        Map<Item, Integer> basket = (Map<Item, Integer>) session.getAttribute("basket");

        if (likes != null && likes.contains(item)) {
            item.setLike(true);
        }
        if (basket != null && basket.containsKey(item)) {
            item.setInBasket(true);
        }
        session.setAttribute("pages", 0);
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(item, HttpStatus.OK);
    }

    @GetMapping(value = "/groupNames")
    public ResponseEntity<List<String>> getGroupNames() {
        List<String> groups = itemService.getGroups();
        return getResponseEntityString(groups);
    }

    @GetMapping(value = "/subgroupNames/{group}")
    public ResponseEntity<List<String>> getSubgroupNames(@PathVariable("group") String group) {
        List<String> subgroups = itemService.getSubgroups(group);
        return getResponseEntityString(subgroups);
    }

    private ResponseEntity<List<String>> getResponseEntityString(List<String> list) {
        if (list.isEmpty()) {
            logger.info("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping(value = "/pages")
    public ResponseEntity<Integer> getPages(HttpSession session) {
        Integer pages = (Integer) session.getAttribute("pages");
        if (pages == null) {
            return new ResponseEntity<Integer>(0, HttpStatus.OK);
        } else {
            return new ResponseEntity<Integer>(pages, HttpStatus.OK);
        }
    }

    @GetMapping(value = "/max-price")
    public ResponseEntity<Double> getMaxPrice(HttpSession session) {
        Double maxPrice = (Double) session.getAttribute("max-price");
        if (maxPrice == null) {
            return new ResponseEntity<Double>(0.0, HttpStatus.OK);
        } else {
            return new ResponseEntity<Double>(maxPrice, HttpStatus.OK);
        }
    }


    @GetMapping(value = "/items/{group}")
    public ResponseEntity<List<Item>> readItemsByGroup(@PathVariable("group") String group,
                                                       @RequestParam(value = "page", required = false) Integer page,
                                                       HttpSession session) {
        if (page == null) { page = 0; }
        logger.info("Read items: group=" + group + " page=" + page);
        Page<Item> pagingItems = itemService.getItems(group, page);
        session.setAttribute("max-price", itemService.getMaxPrice(group));
        return getResponseEntity(pagingItems, session);
    }

    @GetMapping(value = "/items/{group}/{subgroup}")
    public ResponseEntity<List<Item>> readItemsBySubgroup(@PathVariable("group") String group,
                                                          @PathVariable("subgroup") String subgroup,
                                                          @RequestParam(value = "page", required = false) Integer page,
                                                          HttpSession session) {
        if (page == null) { page = 0; }
        logger.info("Read items: group=" + group + " subgroup=" + subgroup + " page=" + page);
        Page<Item> pagingItems = itemService.getItems(group, subgroup, page);
        session.setAttribute("max-price", itemService.getMaxPrice(group, subgroup));
        return getResponseEntity(pagingItems, session);
    }

    @GetMapping(value = "/items/search/{charSequence}")
    public ResponseEntity<List<Item>> searchItems(@PathVariable("charSequence") String charSequence,
                                                  @RequestParam(value = "page", required = false) Integer page,
                                                  HttpSession session) {
        if (page == null) { page = 0; }
        logger.info("Search items: charSequence=" + charSequence + " page=" + page);
        Page<Item> pagingItems = itemService.getItemsBySearch(charSequence, page);
        session.setAttribute("max-price", itemService.getMaxPriceOfSearchItems(charSequence));
        return getResponseEntity(pagingItems, session);
    }


    @GetMapping(value = "/items/{group}/filter")
    public ResponseEntity<List<Item>> readItemsByGroupAndFilter(@PathVariable("group") String group,
                                                                @RequestParam(value = "sort", required = false) String sort,
                                                                @RequestParam(value = "price", required = false) String price,
                                                                @RequestParam(value = "page", required = false) Integer page,
                                                                HttpSession session, @Autowired Filter filter) {
        if (page == null) { page = 0; }
        logger.info("Read items: group=" + group + " sort=" + sort + " price=" + price + " page=" + page);
        filter.parseSort(sort);
        filter.parsePrice(price);
        Page<Item> pagingItems = itemService.getItemsByFilter(group, filter.getMinPrice(), filter.getMaxPrice(), page,
                                                                filter.getSortObj());
        return getResponseEntity(pagingItems, session);
    }


    @GetMapping(value = "/items/{group}/{subgroup}/filter")
    public ResponseEntity<List<Item>> readItemsBySubgroupAndFilter(@PathVariable("group") String group,
                                                                   @PathVariable("subgroup") String subgroup,
                                                                   @RequestParam(value = "sort", required = false) String sort,
                                                                   @RequestParam(value = "price", required = false) String price,
                                                                   @RequestParam(value = "page", required = false) Integer page,
                                                                   HttpSession session, @Autowired Filter filter) {
        if (page == null) { page = 0; }
        logger.info("Read items: group=" + group + " subgroup=" + subgroup + sort + " price=" + price + " page=" + page);
        filter.parseSort(sort);
        filter.parsePrice(price);
        Page<Item> pagingItems = itemService.getItemsByFilter(group, subgroup, filter.getMinPrice(),
                                                                filter.getMaxPrice(), page, filter.getSortObj());
        return getResponseEntity(pagingItems, session);
    }


    @GetMapping(value = "/items/search/{charSequence}/filter")
    public ResponseEntity<List<Item>> searchItems(@PathVariable("charSequence") String charSequence,
                                                  @RequestParam(value = "sort", required = false) String sort,
                                                  @RequestParam(value = "price", required = false) String price,
                                                  @RequestParam(value = "page", required = false) Integer page,
                                                  HttpSession session, @Autowired Filter filter) {
        if (page == null) { page = 0; }
        logger.info("Search items: charSequence=" + charSequence + " page=" + page);
        filter.parseSort(sort);
        filter.parsePrice(price);
        Page<Item> pagingItems = itemService.getItemsBySearchAndFilter(charSequence, filter.getMinPrice(),
                                                            filter.getMaxPrice(), page, filter.getSortObj());
        return getResponseEntity(pagingItems, session);
    }

    private ResponseEntity<List<Item>> getResponseEntity(Page<Item> pagingItems, HttpSession session) {
        if (!pagingItems.hasContent()) {
            logger.info("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        session.setAttribute("pages", pagingItems.getTotalPages());
        List<Item> items = pagingItems.getContent();

        Set<Item> likes = (Set<Item>) session.getAttribute("likes");
        Map<Item, Integer> basket = (Map<Item, Integer>) session.getAttribute("basket");

        for (Item item : items) {
            if (likes != null && likes.contains(item)) {
                item.setLike(true);
            }
            if (basket != null && basket.containsKey(item)) {
                item.setInBasket(true);
            }
        }
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(items, HttpStatus.OK);
    }


}
