package webstore.item.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import webstore.item.FeedBack;
import webstore.item.ItemService;

import javax.validation.Valid;
import java.util.List;

/**
 * GET     /api/v.0.1/feedBack/{itemId}         - read all item's feedbacks         return List<FeedBack>
 * POST    /api/v.0.1/feedBack/{itemId}         - update item's feedbacks
 */
@RestController
@RequestMapping(value = "/api/v.0.1/feedBack/{itemId}", produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemFeedBackController {
    private Logger logger = Logger.getLogger(ItemFeedBackController.class);
    @Autowired
    private ItemService itemService;

    @GetMapping()
    public ResponseEntity<List<FeedBack>> readFeedBack(@PathVariable("itemId") Long itemId) {

        List<FeedBack> feedBacks = itemService.getFeedBacks(itemId);
        logger.info(feedBacks);

        if (feedBacks.isEmpty()) {
            logger.error("HttpStatus: NO_CONTENT");
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(feedBacks, HttpStatus.OK);

    }

    @PostMapping()
    public ResponseEntity<Void> readFeedBack(@RequestBody @Valid FeedBack feedBack, BindingResult bindingResult,
                                             @PathVariable("itemId") Long itemId) {
        if (bindingResult.hasErrors()) {
            logger.error("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (itemService.saveFeedBack(itemId, feedBack)){
            logger.info("HttpStatus: CREATED");
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            logger.error("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }


    }

}
