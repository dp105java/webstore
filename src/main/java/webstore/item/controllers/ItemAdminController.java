package webstore.item.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import webstore.item.Item;
import webstore.item.ItemService;

import javax.validation.Valid;

/**
 * PUT        /api/v.0.1/admin/item              - create or update item
 * DELETE     /api/v.0.1/admin/item/{itemId}     - delete item
 */
@RestController
@RequestMapping(value = "/api/v.0.1/admin/item", produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemAdminController {
    private Logger logger = Logger.getLogger(ItemAdminController.class);
    @Autowired
    private ItemService itemService;

    @PutMapping()
    public ResponseEntity<Void> saveOrUpdateItem(@RequestBody @Valid Item item, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            logger.error("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (itemService.saveOrUpdate(item)) {
            logger.info("HttpStatus: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            logger.error("HttpStatus: BAD_REQUEST");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/{itemId}")
    public ResponseEntity<Void> deleteItem(@PathVariable("itemId") Long itemId) {
        if (itemService.delete(itemId)) {
            logger.info("HttpStatus: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            logger.error("HttpStatus: NOT_FOUND)");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
