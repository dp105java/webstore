package webstore.item.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import webstore.item.Item;
import webstore.item.ItemService;

import javax.servlet.http.HttpSession;
import java.util.HashSet;

import java.util.Set;

/**
 * POST         /api/v.0.1/like/{itemId}        - add item to like list             return List<Item>
 * DELETE       /api/v.0.1/dislike/{itemId}     - remove item from like list        return List<Item>
 * GET          /api/v.0.1/likes                - read all liked items              return List<Item>
 * DELETE       /api/v.0.1/likes                - remove all liked items            return List<Item>
 */
@RestController
@RequestMapping(value = "/api/v.0.1", produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemLikeController {
    private Logger logger = Logger.getLogger(ItemLikeController.class);
    @Autowired
    private ItemService itemService;

    @PostMapping(value = "/like/{itemId}")
    public ResponseEntity<Void> likeItem(@PathVariable("itemId") Long itemId, HttpSession session) {
        Item item = itemService.getById(itemId);
        if (item != null) {
            Set<Item> likes = (Set<Item>) session.getAttribute("likes");
            if (likes == null) {
                likes = new HashSet<>();
            }
            if (!likes.contains(item)) {
                likes.add(item);
                session.setAttribute("likes", likes);
            }

            logger.info("HttpStatus: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        logger.info("HttpStatus: NO_CONTENT");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/dislike/{itemId}")
    public ResponseEntity<Void> dislikeItem(@PathVariable("itemId") Long itemId, HttpSession session) {
        Set<Item> likes = (Set<Item>) session.getAttribute("likes");
        if (likes != null) {
            for (Item item : likes) {
                if (item.getId().equals(itemId)) {
                    likes.remove(item);
                    session.setAttribute("likes", likes);
                    logger.info("HttpStatus: OK");
                    return new ResponseEntity<>(HttpStatus.OK);
                }
            }
        }
        logger.info("HttpStatus: NO_CONTENT");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping(value = "/likes")
    public ResponseEntity<Set<Item>> getLikedItems(HttpSession session) {
        Set<Item> likes = (Set<Item>) session.getAttribute("likes");

        if (likes == null) {
            likes = new HashSet<>();
            session.setAttribute("likes", likes);
        }
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(likes, HttpStatus.OK);
    }

    @DeleteMapping(value = "/likes")
    public ResponseEntity<Void> removeLikedItems(HttpSession session) {
        Set<Item> likes = new HashSet<>();
        session.setAttribute("likes", likes);
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
