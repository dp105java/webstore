package webstore.item.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import webstore.item.Item;
import webstore.item.ItemService;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * POST         /api/v.0.1/basket/{itemId}  - add or increment count of item in basket  return List<Item>
 * DELETE       /api/v.0.1/basket/{itemId}  - remove item from basket                   return List<Item>
 * GET          /api/v.0.1/basket           - read all items from basket                return Map<String, Integer>
 * DELETE       /api/v.0.1/basket           - remove all items from basket              return Map<String, Integer>
 * POST         /api/v.0.1/basket/changeCount/{itemId}/{delta} - change count of item in basket on delta
 */
@RestController
@RequestMapping(value = "/api/v.0.1/basket", produces = MediaType.APPLICATION_JSON_VALUE)
public class ItemBasketController {
    private Logger logger = Logger.getLogger(ItemBasketController.class);
    @Autowired
    private ItemService itemService;
    @Autowired
    private ObjectMapper mapper;

    @PostMapping(value = "/{itemId}")
    public ResponseEntity<Void> putToBasket(@PathVariable("itemId") Long itemId, HttpSession session) {
        Item item = itemService.getById(itemId);
        if (item != null) {
            Map<Item, Integer> basket = (Map<Item, Integer>) session.getAttribute("basket");
            if (basket == null) {
                basket = new HashMap<>();
            }
            basket.put(item, 1);
            session.setAttribute("basket", basket);
            logger.info("HttpStatus: OK");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        logger.info("HttpStatus: NO_CONTENT");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping(value = "/changeCount/{itemId}/{delta}")
    public ResponseEntity<Void> changeCountOfItemInBasket(@PathVariable("itemId") Long itemId,
                                                          @PathVariable("delta") Integer delta,
                                                          HttpSession session) {
        Map<Item, Integer> basket = (Map<Item, Integer>) session.getAttribute("basket");
        if (basket != null) {
            for (Item item : basket.keySet()) {
                if (item.getId().equals(itemId)) {
                    int count = basket.get(item);
                    int newCount = count + delta;
                    newCount = newCount > 1 ? newCount : 1;
                    basket.put(item, newCount);
                    session.setAttribute("basket", basket);
                    logger.info("HttpStatus: OK");
                    return new ResponseEntity<>(HttpStatus.OK);
                }
            }
        }
        logger.info("HttpStatus: NO_CONTENT");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/{itemId}")
    public ResponseEntity<Void> removeFromBasket(@PathVariable("itemId") Long itemId, HttpSession session) {
        Map<Item, Integer> basket = (HashMap<Item, Integer>) session.getAttribute("basket");
        if (basket != null) {
            for (Item item : basket.keySet()) {
                if (item.getId().equals(itemId)) {
                    basket.remove(item);
                    session.setAttribute("basket", basket);
                    logger.info("HttpStatus: OK");
                    return new ResponseEntity<>(HttpStatus.OK);
                }
            }
        }
        logger.error("HttpStatus: NO_CONTENT");
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping()
    public ResponseEntity<Map<String, Integer>> getItemsFromBasket(HttpSession session) throws JsonProcessingException {
        Map<Item, Integer> basket = (HashMap<Item, Integer>) session.getAttribute("basket");
        if (basket == null) {
            logger.info("HttpStatus: OK");
            return new ResponseEntity<>(new HashMap<>(), HttpStatus.OK);
        }
        Map<String, Integer> basketJson = new HashMap<>();
        for (Map.Entry<Item, Integer> b : basket.entrySet()) {
            String itemJson = mapper.writeValueAsString(b.getKey());
            basketJson.put(itemJson, b.getValue());
        }
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(basketJson, HttpStatus.OK);
    }

    @DeleteMapping()
    public ResponseEntity<Map<String, Integer>> removeAllItemsFromBasket(HttpSession session) {
        Map<Item, Integer> basket = new HashMap<>();
        session.setAttribute("basket", basket);
        logger.info("HttpStatus: OK");
        return new ResponseEntity<>(new HashMap<>(), HttpStatus.OK);
    }
}
