package webstore.item;

import org.apache.log4j.Logger;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class Filter {
    private Logger logger = Logger.getLogger(Filter.class);

    private Sort sortObj;
    private int minPrice;
    private int maxPrice;

    public Filter() {
        maxPrice = Integer.MAX_VALUE;
    }

    public Sort getSortObj() {
        return sortObj;
    }

    public void setSortObj(Sort sortObj) {
        this.sortObj = sortObj;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public void parseSort(String sort) {
        if (sort == null) {
            logger.info("Sort is null");
            sortObj = null;
            return;
        }

        String[] s = sort.split("-");
        if (s.length != 2) {
            logger.info("Sort elements more or less than 2");
            sortObj = null;
            return;
        }
        Sort.Direction direction = null;
        ItemFilterProperties property = null;
        try {
            direction = Sort.Direction.valueOf(s[0]);
            property = ItemFilterProperties.valueOf(s[1]);
        } catch (Exception e) {
            try {
                direction = Sort.Direction.valueOf(s[1]);
                property = ItemFilterProperties.valueOf(s[0]);
            } catch (Exception ex) {
                logger.error("invalid value of direction or properties");
                sortObj = null;
                return;
            }
        }
        sortObj = new Sort(direction, property.toString());
    }

    public void parsePrice(String price) {
        if (price == null) {
            logger.error("Price parsing is null");
            minPrice = 0;
            maxPrice = Integer.MAX_VALUE;
            return;
        }

        String[] p = price.split("-");
        if (p.length != 2) {
            logger.info("Price elements more or less than 2");
            minPrice = 0;
            maxPrice = Integer.MAX_VALUE;
            return;
        }

        try {
            minPrice = Integer.parseInt(p[0]);
            maxPrice = Integer.parseInt(p[1]);

            if (minPrice > maxPrice) {
                int temp = maxPrice;
                maxPrice = minPrice;
                minPrice = temp;
            }
        } catch (Exception e) {
            logger.error("invalid value of price");
            minPrice = 0;
            maxPrice = Integer.MAX_VALUE;
            return;
        }
    }

    public enum ItemFilterProperties {
        title,
        averageGrade,
        price
    }
}
