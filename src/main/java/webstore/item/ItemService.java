package webstore.item;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface ItemService {

    boolean saveOrUpdate(Item item);

    boolean delete(Long id);

    Item getById(Long id);

    Page<Item> getItems(String group, int page);

    Page<Item> getItems(String group, String subGroup, int page);

    Page<Item> getItemsByFilter(String group, int min, int max, int pageNumber, Sort sort);

    Page<Item> getItemsByFilter(String group, String subgroup, int min, int max, int pageNumber, Sort sort);

    Page<Item> getItemsBySearch(String charSequence, int page);

    Page<Item> getItemsBySearchAndFilter(String charSequence, int minPrice, int maxPrice, int page, Sort sort);

    double getMaxPrice(String group);

    double getMaxPrice(String group, String subgroup);

    double getMaxPriceOfSearchItems(String charSequence);

    List<String> getGroups();

    List<String> getSubgroups(String group);

    List<FeedBack> getFeedBacks(Long id);

    boolean saveFeedBack(Long id, FeedBack feedBack);
}

