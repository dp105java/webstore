package webstore.item;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(readOnly = true)
public class ItemServiceImpl implements ItemService {
    private Logger logger = Logger.getLogger(ItemServiceImpl.class);

    @Autowired
    private ItemRepository itemRepository;
    private Sort defaultSort = new Sort(new Sort.Order(Sort.Direction.DESC, "averageGrade"),
            new Sort.Order(Sort.Direction.ASC, "title"));
    public static final int PAGE_SIZE = 9;


    /*
     *  When item was updated, List<FeedBack> and averageGrade should not be updated.
     */
    @Override
    @Transactional
    public boolean saveOrUpdate(Item item) {
        if (item == null) {
            logger.error("Item is null. Saving unsuccessfully.");
            return false;
        }

        if (item.getId() == null) {
            itemRepository.save(item);
            logger.info(item + "was saved successfully");
            return true;
        }

        Item itemDB = itemRepository.findOne(item.getId());
        if (itemDB != null) {
            item.setAverageGrade(itemDB.getAverageGrade());
            item.setFeedBacks(itemDB.getFeedBacks());
            itemRepository.save(item);
            logger.info(item + "was update successfully");
            return true;
        }

        logger.error(item + "was not saved or update");
        return false;
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        if (id == null) {
            logger.error("Id is null. Deleting unsuccessfully.");
            return false;
        }

        if (itemRepository.findOne(id) == null) {
            logger.info("Deleting item doesn't exist.");
            return false;
        }

        itemRepository.delete(id);
        logger.info("Item #" + id + "was deleted successfully");
        return true;
    }

    @Override
    public Item getById(Long id) {
        if (id == null) {
            logger.error("Id is null. Getting unsuccessfully.");
            return null;
        }

        Item item = itemRepository.findOne(id);
        if (item == null) {
            logger.info("Item does not exist");
            return null;
        }
        item.getFeedBacks().isEmpty();
        item.setLike(false);
        item.setInBasket(false);
        logger.info(item + "was gotten successfully");
        return item;
    }

    @Override
    public Page<Item> getItems(String group, int pageNumber) {
        if (pageNumber < 0) {
            logger.error("page less then 0");
            return new PageImpl<Item>(new ArrayList<>());
        }

        PageRequest pageRequest = new PageRequest(pageNumber, PAGE_SIZE, defaultSort);
        Page<Item> items = itemRepository.findByGroupName(group, pageRequest);
        logger.info(group + "was gotten successfully. page :" + pageNumber);
        return items;
    }

    @Override
    public Page<Item> getItems(String group, String subGroup, int pageNumber) {
        if (pageNumber < 0) {
            logger.error("page less then 0");
            return new PageImpl<Item>(new ArrayList<>());
        }
        PageRequest pageRequest = new PageRequest(pageNumber, PAGE_SIZE, defaultSort);
        Page<Item> items = itemRepository.findByGroupNameAndSubgroupName(group, subGroup, pageRequest);
        logger.info(subGroup + "was gotten successfully. page :" + pageNumber);
        return items;
    }

    @Override
    public Page<Item> getItemsByFilter(String group, int min, int max, int pageNumber, Sort sort) {
        if (pageNumber < 0) {
            logger.error("page less then 0");
            return new PageImpl<Item>(new ArrayList<>());
        }
        if (sort == null) {
            sort = defaultSort;
        }

        try {
            PageRequest pageRequest = new PageRequest(pageNumber, PAGE_SIZE, sort);
            Page<Item> items = itemRepository.findByFilter(group, min, max, pageRequest);
            logger.info(group + "was gotten successfully with filter. page :" + pageNumber);
            return items;
        } catch (InvalidDataAccessApiUsageException e) {
            logger.error(e);
            return new PageImpl<Item>(new ArrayList<>());
        }

    }

    @Override
    public Page<Item> getItemsByFilter(String group, String subgroup, int min, int max, int page, Sort sort) {
        if (page < 0) {
            logger.error("page less then 0");
            return new PageImpl<Item>(new ArrayList<>());
        }
        if (sort == null) {
            sort = defaultSort;
        }
        try {
            PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, sort);
            Page<Item> items = itemRepository.findByFilter(group, subgroup, min, max, pageRequest);
            logger.info(subgroup + "was gotten successfully with filter. page :" + page);
            return items;
        } catch (InvalidDataAccessApiUsageException e) {
            logger.error(e);
            return new PageImpl<Item>(new ArrayList<>());
        }
    }

    @Override
    public Page<Item> getItemsBySearch(String charSequence, int page) {
        if (charSequence == null) {
            logger.error("charSequence is null");
            return new PageImpl<Item>(new ArrayList<>());
        }
        if (page < 0) {
            logger.error("page less then 0");
            return new PageImpl<Item>(new ArrayList<>());
        }

        PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, defaultSort);
        Page<Item> items = itemRepository.findBySearch(charSequence.toLowerCase(), pageRequest);
        logger.info(charSequence + "was gotten successfully. page :" + page);
        return items;
    }

    @Override
    public Page<Item> getItemsBySearchAndFilter(String charSequence, int minPrice, int maxPrice, int page, Sort sort) {
        if (charSequence == null) {
            logger.error("charSequence is null");
            return new PageImpl<Item>(new ArrayList<>());
        }
        if (page < 0) {
            logger.error("page less then 0");
            return new PageImpl<Item>(new ArrayList<>());
        }
        try {
            PageRequest pageRequest = new PageRequest(page, PAGE_SIZE, sort);
            Page<Item> items = itemRepository.findBySearch(charSequence.toLowerCase(), minPrice, maxPrice, pageRequest);
            logger.info(charSequence + "was gotten successfully. page :" + page);
            return items;
        } catch (InvalidDataAccessApiUsageException e) {
            logger.error(e);
            return new PageImpl<Item>(new ArrayList<>());
        }
    }

    @Override
    public double getMaxPrice(String group) {
        Double maxPrice = itemRepository.findMaxPrice(group);
        return maxPrice == null ? 0 : maxPrice;
    }

    @Override
    public double getMaxPrice(String group, String subgroup) {
        Double maxPrice = itemRepository.findMaxPrice(group, subgroup);
        return maxPrice == null ? 0 : maxPrice;
    }

    @Override
    public double getMaxPriceOfSearchItems(String charSequence) {
        if (charSequence == null) { return 0; }
        Double maxPriceBySearch = itemRepository.findMaxPriceBySearch(charSequence.toLowerCase());
        return maxPriceBySearch == null ? 0 : maxPriceBySearch;
    }

    @Override
    public List<String> getGroups() {
        List<String> groups = itemRepository.findGroups();
        Collections.sort(groups);
        logger.info("groups gotten successfully");
        return groups;
    }

    @Override
    public List<String> getSubgroups(String group) {
        if (group == null) {
            logger.error("group is null");
            return new ArrayList<>();
        }
        List<String> subGroups = itemRepository.findSubgroups(group);
        Collections.sort(subGroups);
        logger.info("subGroups gotten successfully");
        return subGroups;
    }


    @Override
    public List<FeedBack> getFeedBacks(Long id) {
        if (id == null) {
            logger.error("itemId is null");
            return new ArrayList<>();
        }
        Item item = itemRepository.findOne(id);
        if (item == null) {
            logger.info("item does not exist");
            return new ArrayList<>();
        }
        item.getFeedBacks().isEmpty();
        return item.getFeedBacks();
    }

    @Override
    @Transactional
    public boolean saveFeedBack(Long id, FeedBack feedBack) {
        if (id == null || itemRepository.findOne(id) == null || feedBack == null) {
            logger.error("itemId or feedBack is null");
            return false;
        }
        Item item = itemRepository.findOne(id);
        item.getFeedBacks().add(feedBack);
        updateGrade(item);
        itemRepository.save(item);
        logger.info(item + "was saved successfully");
        return true;
    }


    private void updateGrade(Item item) {
        double sum = 0;
        int count = 0;
        for (FeedBack f : item.getFeedBacks()) {
            sum += f.getGrade();
            count++;
        }
        item.setAverageGrade(sum / count);
    }
}
