package webstore.order;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Embeddable
public class Purchase {
    @NotNull
    @Column(nullable = false)
    private Long itemId;
    @Min(0)
    @NotNull
    @Column(nullable = false)
    private Double price;
    @Min(1)
    private int count;


    public Purchase() {
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
