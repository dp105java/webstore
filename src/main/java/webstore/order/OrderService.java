package webstore.order;

import webstore.customer.Customer;
import webstore.delivery.Address;
import webstore.item.Item;
import webstore.payment.Payment;

import java.util.HashMap;

public interface OrderService {

    boolean addPurchases(HashMap<Item, Integer> basket);

    boolean addCustomer(Customer customer);

    boolean setDeliveryAddress(Address address);

    boolean setPayment (Payment payment);

    boolean checkout();


}
